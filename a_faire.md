# Liste des choses a faire

## Synoptique simplifier

```mermaid
graph LR;
  144MHz  --> ATT;
  ATT     --> MIXER;
  OL      --> AMP_OL;
  AMP_OL  --> FILTRE_OL;
  FILTRE_OL--> MIXER;
  MIXER   --> FILTRE;
  FILTRE  --> AMP1_TX;
  FILTRE  --> AMP1_RX;
  AMP1_RX --> LNA_RX;
  LNA_RX  --> ANT;
  AMP1_TX --> AMP2_TX;
  AMP2_TX --> ANT;
```



## Module
### Aplificateur FI
  - [x] Choix composant		-> ERA-5SM
  - [x] Simulation			-> OK
  - [x] Conception PCB		-> OK
  - [x] Fabrication/Montage	-> OK
  - [x] Mesure 				-> OK
  - [x] Validation			-> OK

### Mélangeur
  - [x] Choix composant		-> ADE-25
  - [x] Simulation			-> OK
  - [x] Conception PCB		-> OK
  - [x] Fabrication/Montage	-> OK
  - [x] Mesure 				-> OK
  - [x] Validation			-> OK

### OL
  - [x] Choix composant		-> ADF4350
  - [x] Logiciel Arduino	-> OK
  - [x] Conception PCB		-> OK
  - [x] Fabrication/Montage	-> OK
  - [x] Mesure 				-> pas recup les courbes
  - [x] Validation			-> OK

  - Choix a faire sur la strategie
    - Montage attenuateur fixe ou numerique
    - Montage prescaler pour le bruit de Phase

### Interrupteur
  - [x] Choix composant		-> AS179-92LF
  - [x] Conception PCB		-> OK
  - [ ] Fabrication/Montage	->
  - [ ] Mesure 				->
  - [ ] Validation			->

### LNA
  - [x] Choix composant		-> SPF5189Z
  - [x] Simulation			-> OK
  - [x] Conception PCB		-> OK
  - [x] Fabrication/Montage	-> OK
  - [x] Mesure 				-> OK
  - [x] Validation			->
	- [ ] NF relativement haut.
		- 1,6mm pas terrible
		- microstrip trop large pour les capa 0603
		- inductance ?
		- empreinte LNA trop grande

### Filtre
 1. Ligne couplé + capa
    - [x] Simulation			-> OK
    - [x] Conception PCB		-> OK
    - [x] Fabrication/Montage	-> OK
    - [x] Mesure 				-> Attention position capa
    - [x] Validation			-> OK voir des capa variable
 2. PinHair
    - [x] Simulation			-> OK
    - [x] Conception PCB		-> OK
    - [ ] Fabrication/Montage  ->
    - [ ] Mesure 				->
    - [ ] Validation			->


### Gestion des tensions
 - [ ] Toutes les modules ne fonctionne pas a la même tension
 - [ ] prevoir : 3.3V | 5V | 7V | 12V
 - [X] regul abaisseur a découpage
 - [X] regul AMS1117 5V
 - [X] regul AMS1117 3.3V
 - [X] regul 78L08

### Gestion PTT
 - [ ] Quoi/comment

## ACHAT
 - Boitier pour l'OL
 - Boitier pour le transverter
 - Attenuateur
 - connecteur SMA M-M
 - 


