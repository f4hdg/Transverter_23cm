EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_Coaxial J2
U 1 1 5E1F809F
P 6000 3450
F 0 "J2" H 6100 3425 50  0000 L CNN
F 1 "Conn_Coaxial" H 6100 3334 50  0000 L CNN
F 2 "lib_kicad:SMA" H 6000 3450 50  0001 C CNN
F 3 " ~" H 6000 3450 50  0001 C CNN
	1    6000 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E1F854D
P 6000 3650
F 0 "#PWR0101" H 6000 3400 50  0001 C CNN
F 1 "GND" H 6005 3477 50  0000 C CNN
F 2 "" H 6000 3650 50  0001 C CNN
F 3 "" H 6000 3650 50  0001 C CNN
	1    6000 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5E1F9271
P 5350 3450
F 0 "J1" H 5278 3688 50  0000 C CNN
F 1 "Conn_Coaxial" H 5278 3597 50  0000 C CNN
F 2 "lib_kicad:SMA" H 5350 3450 50  0001 C CNN
F 3 " ~" H 5350 3450 50  0001 C CNN
	1    5350 3450
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E1F9277
P 5350 3650
F 0 "#PWR0102" H 5350 3400 50  0001 C CNN
F 1 "GND" H 5355 3477 50  0000 C CNN
F 2 "" H 5350 3650 50  0001 C CNN
F 3 "" H 5350 3650 50  0001 C CNN
	1    5350 3650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5550 3450 5800 3450
$EndSCHEMATC
