<Qucs Schematic 0.0.20>
<Properties>
  <View=0,60,1265,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=LPF_1800MHZ_mesure.dat>
  <DataDisplay=LPF_1800MHZ_mesure.dpl>
  <OpenDisplay=1>
  <Script=LPF_1800MHZ_mesure.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 180 290 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *1 5 180 320 0 0 0 0>
  <Pac P2 1 860 290 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *2 5 860 320 0 0 0 0>
  <.SP SP1 1 190 420 0 67 0 0 "lin" 1 "20 MHz" 1 "5 GHz" 1 "499" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <SUBST Sub1 1 420 460 -30 24 0 0 "4.2" 1 "600 um" 1 "35 um" 1 "0.035" 1 "1e-10" 1 "0" 1>
  <Eqn Eqn1 1 570 520 -28 15 0 0 "S21_dB=dB(S[2,1])" 1 "S11_dB=dB(S[1,1])" 1 "yes" 0>
  <GND * 5 490 200 0 0 0 0>
  <SPfile X1 1 490 140 -26 -59 0 0 "/home/nop/Radioamateur/git/Transverter_23cm/Filtre/LPF_1.8GHz/Mesure/2020-01-20-LPF-23cm.s2p" 1 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
</Components>
<Wires>
  <860 140 860 260 "" 0 0 0 "">
  <520 140 860 140 "" 0 0 0 "">
  <180 140 180 260 "" 0 0 0 "">
  <180 140 460 140 "" 0 0 0 "">
  <490 170 490 200 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Text 550 420 12 #000000 0 "stepped-impedance lowpass filter \n Butterworth 1.8 GHz...2 GHz \n impedance matching 50 Ohm">
</Paintings>
