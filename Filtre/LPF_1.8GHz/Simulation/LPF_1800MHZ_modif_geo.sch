<Qucs Schematic 0.0.20>
<Properties>
  <View=0,119,954,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=LPF_1800MHZ_modif_geo.dat>
  <DataDisplay=LPF_1800MHZ_modif_geo.dpl>
  <OpenDisplay=1>
  <Script=LPF_1800MHZ_modif_geo.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 140 300 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *1 5 140 330 0 0 0 0>
  <MLIN MS1 1 200 150 -26 15 0 0 "Sub1" 1 "500 um" 1 "4.5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS2 1 290 150 -26 15 0 0 "Sub1" 1 "20.5 mm" 1 "1.5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS3 1 380 150 -26 15 0 0 "Sub1" 1 "500 um" 1 "18 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS4 1 470 150 -26 15 0 0 "Sub1" 1 "20.5 mm" 1 "2.5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS5 1 560 150 -26 15 0 0 "Sub1" 1 "500 um" 1 "18 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS6 1 650 150 -26 15 0 0 "Sub1" 1 "20.5 mm" 1 "1.5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS7 1 740 150 -26 15 0 0 "Sub1" 1 "500 um" 1 "4.5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P2 1 820 300 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *2 5 820 330 0 0 0 0>
  <.SP SP1 1 150 430 0 67 0 0 "lin" 1 "20 MHz" 1 "5 GHz" 1 "499" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 530 530 -28 15 0 0 "S21_dB=dB(S[2,1])" 1 "S11_dB=dB(S[1,1])" 1 "yes" 0>
  <SUBST Sub1 1 380 470 -30 24 0 0 "4.2" 1 "600 um" 1 "35 um" 1 "0.035" 1 "1e-10" 1 "0" 1>
</Components>
<Wires>
  <140 150 140 270 "" 0 0 0 "">
  <140 150 170 150 "" 0 0 0 "">
  <820 150 820 270 "" 0 0 0 "">
  <770 150 820 150 "" 0 0 0 "">
  <230 150 260 150 "" 0 0 0 "">
  <320 150 350 150 "" 0 0 0 "">
  <410 150 440 150 "" 0 0 0 "">
  <500 150 530 150 "" 0 0 0 "">
  <590 150 620 150 "" 0 0 0 "">
  <680 150 710 150 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Text 510 430 12 #000000 0 "stepped-impedance lowpass filter \n Butterworth 1.8 GHz...2 GHz \n impedance matching 50 Ohm">
</Paintings>
