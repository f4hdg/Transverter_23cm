<Qucs Schematic 0.0.20>
<Properties>
  <View=0,0,934,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=LPF_1800MHZ_auto.dat>
  <DataDisplay=LPF_1800MHZ_auto.dpl>
  <OpenDisplay=1>
  <Script=LPF_1800MHZ_auto.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 120 280 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *1 5 120 310 0 0 0 0>
  <MLIN MS1 1 180 130 -26 15 0 0 "Sub1" 1 "500 um" 1 "4.451 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS2 1 270 130 -26 15 0 0 "Sub1" 1 "20.5 mm" 1 "1.573 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS3 1 360 130 -26 15 0 0 "Sub1" 1 "500 um" 1 "18.11 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS4 1 450 130 -26 15 0 0 "Sub1" 1 "20.5 mm" 1 "2.576 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS5 1 540 130 -26 15 0 0 "Sub1" 1 "500 um" 1 "18.11 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS6 1 630 130 -26 15 0 0 "Sub1" 1 "20.5 mm" 1 "1.573 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS7 1 720 130 -26 15 0 0 "Sub1" 1 "500 um" 1 "4.451 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P2 1 800 280 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *2 5 800 310 0 0 0 0>
  <.SP SP1 1 130 410 0 67 0 0 "lin" 1 "20 MHz" 1 "5 GHz" 1 "499" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <SUBST Sub1 1 360 450 -30 24 0 0 "4.2" 1 "600 um" 1 "35 um" 1 "0.035" 1 "1e-10" 1 "0" 1>
  <Eqn Eqn1 1 510 510 -28 15 0 0 "S21_dB=dB(S[2,1])" 1 "S11_dB=dB(S[1,1])" 1 "yes" 0>
</Components>
<Wires>
  <120 130 120 250 "" 0 0 0 "">
  <120 130 150 130 "" 0 0 0 "">
  <800 130 800 250 "" 0 0 0 "">
  <750 130 800 130 "" 0 0 0 "">
  <210 130 240 130 "" 0 0 0 "">
  <300 130 330 130 "" 0 0 0 "">
  <390 130 420 130 "" 0 0 0 "">
  <480 130 510 130 "" 0 0 0 "">
  <570 130 600 130 "" 0 0 0 "">
  <660 130 690 130 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Text 490 410 12 #000000 0 "stepped-impedance lowpass filter \n Butterworth 1.8 GHz...2 GHz \n impedance matching 50 Ohm">
</Paintings>
