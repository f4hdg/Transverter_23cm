<Qucs Schematic 0.0.19>
<Properties>
  <View=-115,46,904,800,1,0,60>
  <Grid=10,10,1>
  <DataSet=Filtre_1296_test_divers.dat>
  <DataDisplay=Filtre_1296_test_divers.dpl>
  <OpenDisplay=1>
  <Script=Filtre_1296_test_divers.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Eqn Eqn1 1 690 170 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <.SW SW2 1 510 470 0 59 0 0 "SW1" 1 "lin" 1 "var2" 1 "0.5 m" 1 "20 m" 1 "5" 1>
  <Pac P1 1 130 340 -74 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 130 370 0 0 0 0>
  <Eqn Eqn2 1 120 520 -31 15 0 0 "Largeur_1=var" 1 "Longeur_1=5 m" 1 "Distance_1=0.5 m" 1 "yes" 0>
  <Eqn Eqn3 1 280 520 -31 15 0 0 "Largeur_2=1 m" 1 "Longeur_2=5 m " 1 "Distance_2=var" 1 "yes" 0>
  <Eqn Eqn4 1 -40 510 -31 15 0 0 "Largeur_adapt=2 m" 1 "Longeur_adapt=57 m" 1 "yes" 0>
  <MLIN MS1 1 210 280 -26 15 0 0 "Subst1" 1 "Largeur_1" 1 "Longeur_1" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <.SP SPTC1 1 110 90 0 59 0 0 "log" 1 "5 MHz" 1 "3 GHz" 1 "100" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <MLIN MS4 1 300 280 -26 -91 0 2 "Subst1" 1 "Largeur_2" 1 "Longeur_2" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS3 1 380 280 -26 15 0 0 "Subst1" 1 "Largeur_1" 1 "Longeur_1" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <SUBST Subst1 1 -50 140 -30 24 0 0 "4.6" 1 "0.8 mm" 1 "40 um" 1 "0" 1 "2.43902e-08" 1 "0" 1>
  <MLIN MS5 1 470 280 -26 -91 0 2 "Subst1" 1 "Largeur_2" 1 "Longeur_2" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P2 1 770 300 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 770 330 0 0 0 0>
  <.SW SW1 1 730 460 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "1m" 1 "20 m" 1 "5" 1>
  <MLIN MS6 1 640 280 -26 15 0 0 "Subst1" 1 "Largeur_1" 1 "Longeur_1" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
</Components>
<Wires>
  <130 280 130 310 "" 0 0 0 "">
  <130 280 180 280 "" 0 0 0 "">
  <240 280 270 280 "" 0 0 0 "">
  <410 280 440 280 "" 0 0 0 "">
  <330 280 350 280 "" 0 0 0 "">
  <770 270 770 280 "" 0 0 0 "">
  <670 280 770 280 "" 0 0 0 "">
  <500 280 610 280 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
