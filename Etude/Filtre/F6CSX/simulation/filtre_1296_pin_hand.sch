<Qucs Schematic 0.0.19>
<Properties>
  <View=-56,0,990,800,1,17,0>
  <Grid=10,10,1>
  <DataSet=filtre_1296_pin_hand.dat>
  <DataDisplay=filtre_1296_pin_hand.dpl>
  <OpenDisplay=1>
  <Script=filtre_1296_pin_hand.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Eqn Eqn1 1 650 130 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <.SW SW1 1 650 300 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "0.5m" 1 "3 m" 1 "10" 1>
  <Pac P1 1 90 300 -74 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 90 330 0 0 0 0>
  <Eqn Eqn2 1 80 480 -31 15 0 0 "Largeur_1=2 m" 1 "Longeur_1=57 m" 1 "Distance_1=0.5 m" 1 "yes" 0>
  <MOPEN MS3 1 470 300 31 -41 0 0 "Subst1" 1 "Largeur_2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <Eqn Eqn3 1 240 480 -31 15 0 0 "Largeur_2=var" 1 "Longeur_2=57 m" 1 "Distance_2=var" 1 "yes" 0>
  <MCOUPLED MSTC1 1 270 320 -59 -187 0 3 "Subst1" 1 "Largeur_1" 1 "Longeur_1" 1 "Distance_1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MCOUPLED MSTC2 1 390 320 20 -133 0 1 "Subst1" 1 "Largeur_2" 1 "Longeur_2" 1 "Distance_2" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS1 1 300 420 -117 -30 0 3 "Subst1" 1 "Largeur_1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MLIN MS5 1 330 290 -39 -94 0 0 "Subst1" 1 "1 mm" 1 "10 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P2 1 550 380 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 550 410 0 0 0 0>
  <.SW SW2 1 830 290 0 59 0 0 "SW1" 1 "lin" 1 "var2" 1 "1 m" 1 "60 m" 1 "10" 1>
  <SUBST Subst1 1 870 70 -30 24 0 0 "4.6" 1 "0.8 mm" 1 "40 um" 1 "0" 1 "2.43902e-08" 1 "0" 1>
  <.SP SPTC1 1 70 10 0 59 0 0 "log" 1 "0.1 GHz" 1 "10 GHz" 1 "51" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <MOPEN MS4 1 360 430 22 24 0 3 "Subst1" 1 "Largeur_2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS6 1 240 380 -165 26 0 3 "Subst1" 1 "Largeur_1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MLIN MS7 1 210 290 -87 -142 0 0 "Subst1" 1 "1 mm" 1 "10 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS8 1 150 350 -166 -87 0 1 "Subst1" 1 "1 mm" 1 "10 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS9 1 150 410 -165 26 0 3 "Subst1" 1 "Largeur_1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
</Components>
<Wires>
  <420 290 420 300 "" 0 0 0 "">
  <420 300 440 300 "" 0 0 0 "">
  <400 290 420 290 "" 0 0 0 "">
  <300 350 300 390 "" 0 0 0 "">
  <420 350 550 350 "" 0 0 0 "">
  <360 350 360 400 "" 0 0 0 "">
  <150 290 150 320 "" 0 0 0 "">
  <150 290 180 290 "" 0 0 0 "">
  <90 270 90 290 "" 0 0 0 "">
  <90 290 150 290 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
