<Qucs Schematic 0.0.19>
<Properties>
  <View=-140,-377,1044,869,0.981494,32,604>
  <Grid=10,10,1>
  <DataSet=Filtre_1296_type_F6CSX.dat>
  <DataDisplay=Filtre_1296_type_F6CSX.dpl>
  <OpenDisplay=1>
  <Script=Filtre_1296_type_F6CSX.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 270 370 -74 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 270 400 0 0 0 0>
  <MLIN MS1 1 350 310 -26 15 0 0 "Subst1" 1 "Largeur_adapt" 1 "Longeur_adapt" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCOUPLED MS7 1 480 370 37 -26 0 1 "Subst1" 1 "Largeur_1" 1 "Longeur_12" 1 "Distance_1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <.SP SPTC1 1 -90 360 0 61 0 0 "log" 1 "500 MHz" 1 "2.5 GHz" 1 "200" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <MCOUPLED MS8 1 480 220 37 -26 0 1 "Subst1" 1 "Largeur_1" 1 "Longeur_11" 1 "Distance_1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <GND * 1 510 520 0 0 0 0>
  <Eqn Eqn4 1 -50 570 -31 15 0 0 "Largeur_adapt=1.45 m" 1 "Longeur_adapt=15.5 m" 1 "yes" 0>
  <Eqn Eqn5 1 140 560 -31 15 0 0 "Condo=2.7 p * var" 1 "yes" 0>
  <MLIN MS11 1 590 -150 -26 15 0 0 "Subst1" 1 "Largeur_adapt" 1 "Longeur_adapt" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCOUPLED MS12 1 700 -80 37 -26 0 1 "Subst1" 1 "Largeur_1" 1 "Longeur_12" 1 "Distance_1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MVIA MS13 1 670 -280 0 -77 0 1 "Subst1" 1 "1 mm" 1 "26.85" 0>
  <MVIA MS14 1 730 -280 0 -77 0 1 "Subst1" 1 "1 mm" 1 "26.85" 0>
  <MCOUPLED MS15 1 700 -230 37 -26 0 1 "Subst1" 1 "Largeur_1" 1 "Longeur_11" 1 "Distance_1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <GND * 1 670 70 0 0 0 0>
  <GND * 1 730 70 0 0 0 0>
  <C C4 1 670 40 17 -26 0 1 "Condo" 1 "" 0 "neutral" 0>
  <C C5 1 730 40 17 -26 0 1 "Condo" 1 "" 0 "neutral" 0>
  <Eqn Eqn2 1 300 550 -31 15 0 0 "Largeur_1=1.4 m" 1 "Longeur_tot=15.5 m" 1 "Longeur_11=Longeur_tot * 0.25 " 1 "Longeur_12=Longeur_tot * (1- 0.25) " 1 "Distance_1=3 m" 1 "yes" 0>
  <MVIA MS10 1 540 170 0 -77 0 1 "Subst1" 1 "1 mm" 1 "26.85" 0>
  <MVIA MS9 1 380 190 -120 0 1 2 "Subst1" 1 "1 mm" 1 "26.85" 0>
  <C C3 1 510 470 17 -26 0 1 "Condo" 1 "" 0 "neutral" 0>
  <GND * 1 450 540 0 0 0 0>
  <C C1 1 450 510 17 -26 0 1 "Condo" 1 "" 0 "neutral" 0>
  <MLIN MS6 1 700 300 -26 15 0 0 "Subst1" 1 "Largeur_adapt" 1 "Longeur_adapt" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P2 1 850 330 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 850 360 0 0 0 0>
  <Eqn Eqn1 1 80 440 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <.SW SW1 1 610 540 0 61 0 0 "SPTC1" 1 "lin" 1 "var" 1 "0.9" 1 "1.1" 1 "5" 1>
  <.SW SW2 1 780 530 0 61 0 0 "SW1" 1 "lin" 1 "var2" 1 "-1m" 1 "1m" 1 "5" 1>
  <.SW SW3 1 100 660 0 61 0 0 "SW2" 1 "list" 1 "Condo_var" 1 "0.95" 0 "1.05" 0 "[1.8p ; 2.2 p ; 2.7 p]" 1>
  <SUBST Subst1 1 90 170 -30 24 0 0 "4.6" 1 "0.8 mm" 1 "35 um" 1 "0.02" 1 "1.72e-8" 1 "0" 1>
</Components>
<Wires>
  <270 310 270 340 "" 0 0 0 "">
  <270 310 320 310 "" 0 0 0 "">
  <380 310 450 310 "" 0 0 0 "">
  <450 310 450 340 "" 0 0 0 "">
  <510 300 510 340 "" 0 0 0 "">
  <510 250 510 300 "" 0 0 0 "">
  <450 250 450 310 "" 0 0 0 "">
  <620 -150 670 -150 "" 0 0 0 "">
  <620 -200 620 -150 "" 0 0 0 "">
  <620 -200 670 -200 "" 0 0 0 "">
  <670 -150 670 -110 "" 0 0 0 "">
  <730 -50 730 10 "" 0 0 0 "">
  <670 -50 670 10 "" 0 0 0 "">
  <730 -200 730 -150 "" 0 0 0 "">
  <730 -150 730 -110 "" 0 0 0 "">
  <730 -150 820 -150 "" 0 0 0 "">
  <510 190 540 190 "" 0 0 0 "">
  <400 190 450 190 "" 0 0 0 "">
  <510 400 510 440 "" 0 0 0 "">
  <510 500 510 520 "" 0 0 0 "">
  <450 400 450 480 "" 0 0 0 "">
  <510 300 670 300 "" 0 0 0 "">
  <730 300 850 300 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
