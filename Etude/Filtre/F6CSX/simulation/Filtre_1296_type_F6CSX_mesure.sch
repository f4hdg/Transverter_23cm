<Qucs Schematic 0.0.19>
<Properties>
  <View=-60,260,1360,1022,1,60,0>
  <Grid=10,10,1>
  <DataSet=Filtre_1296_type_F6CSX_mesure.dat>
  <DataDisplay=Filtre_1296_type_F6CSX_mesure.dpl>
  <OpenDisplay=1>
  <Script=Filtre_1296_type_F6CSX_mesure.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 350 580 -74 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 350 610 0 0 0 0>
  <Pac P2 1 550 550 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 550 580 0 0 0 0>
  <Eqn Eqn1 1 740 420 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <.SW SW1 1 700 550 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "0.9" 1 "1.1" 1 "5" 1>
  <Eqn Eqn3 1 140 410 -31 15 0 0 "Largeur_adapt=1.45 m" 1 "Longeur_adapt=15.5 m" 1 "yes" 0>
  <.SP SPTC1 1 90 690 0 59 0 0 "log" 1 "500 MHz" 1 "2.5 GHz" 1 "100" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <SPfile X1 1 430 520 -26 -59 0 0 "/home/nop/Radioamateur/git/Transverter_23cm/Filtre_23cm_F6CSX/Mesure/18-03-16_filtre23cm_250MHz-4GHz_2.7pF.s2p" 1 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <GND * 1 430 550 0 0 0 0>
</Components>
<Wires>
  <460 520 550 520 "" 0 0 0 "">
  <350 520 350 550 "" 0 0 0 "">
  <350 520 400 520 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
