<Qucs Schematic 0.0.19>
<Properties>
  <View=-200,-94,800,800,1,0,44>
  <Grid=10,10,1>
  <DataSet=filtre_1296_ligne_couplet.dat>
  <DataDisplay=filtre_1296_ligne_couplet.dpl>
  <OpenDisplay=1>
  <Script=filtre_1296_ligne_couplet.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SPTC1 1 20 -40 0 59 0 0 "log" 1 "0.1 GHz" 1 "10 GHz" 1 "51" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <SUBST Subst1 1 280 -30 -30 24 0 0 "4.6" 1 "0.8 mm" 1 "40 um" 1 "0" 1 "2.43902e-08" 1 "0" 1>
  <Eqn Eqn1 1 470 30 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <.SW SW1 1 470 200 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "0.5m" 1 "3 m" 1 "10" 1>
  <.SW SW2 1 290 330 0 59 0 0 "SW1" 1 "lin" 1 "var2" 1 "1 m" 1 "60 m" 1 "10" 1>
  <MCOUPLED MSTC1 1 70 170 -26 37 0 0 "Subst1" 1 "Largeur_1" 1 "Longeur_1" 1 "Distance_1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <Pac P1 1 -90 200 -74 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 -90 230 0 0 0 0>
  <MOPEN MS1 1 130 140 31 -41 0 0 "Subst1" 1 "Largeur_1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS2 1 10 200 -84 15 0 2 "Subst1" 1 "Largeur_1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <Eqn Eqn2 1 -100 380 -31 15 0 0 "Largeur_1=2 m" 1 "Longeur_1=57 m" 1 "Distance_1=0.5 m" 1 "yes" 0>
  <Pac P2 1 370 230 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 370 260 0 0 0 0>
  <MCOUPLED MSTC2 1 230 230 -26 37 0 0 "Subst1" 1 "Largeur_2" 1 "Longeur_2" 1 "Distance_2" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS3 1 290 200 31 -41 0 0 "Subst1" 1 "Largeur_2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS4 1 170 260 -84 15 0 2 "Subst1" 1 "Largeur_2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <Eqn Eqn3 1 60 380 -31 15 0 0 "Largeur_2=var" 1 "Longeur_2=57 m" 1 "Distance_2=var" 1 "yes" 0>
</Components>
<Wires>
  <-90 140 40 140 "" 0 0 0 "">
  <-90 140 -90 170 "" 0 0 0 "">
  <100 200 200 200 "" 0 0 0 "">
  <260 240 260 260 "" 0 0 0 "">
  <260 240 330 240 "" 0 0 0 "">
  <330 200 330 240 "" 0 0 0 "">
  <330 200 370 200 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
