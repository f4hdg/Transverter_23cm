<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-300,800,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=Filtre_1296_50ohm.dat>
  <DataDisplay=Filtre_1296_50ohm.dpl>
  <OpenDisplay=1>
  <Script=Filtre_1296_50ohm.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 180 280 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 180 310 0 0 0 0>
  <GND * 1 290 310 0 0 0 0>
  <L L1 1 360 200 -26 10 0 0 "7.9617 nH" 1 "" 0>
  <GND * 1 430 310 0 0 0 0>
  <L L2 1 500 200 -26 10 0 0 "7.9617 nH" 1 "" 0>
  <GND * 1 570 310 0 0 0 0>
  <Pac P2 1 680 280 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 680 310 0 0 0 0>
  <Eqn Eqn1 1 410 390 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <C C1 1 290 280 17 -26 0 1 "4.4158 pF" 1 "" 0 "neutral" 0>
  <C C3 1 570 280 17 -26 0 1 "4.4158 pF" 1 "" 0 "neutral" 0>
  <C C2 1 430 280 17 -26 0 1 "6.5783 pF" 1 "" 0 "neutral" 0>
  <.SP SP1 1 170 350 0 67 0 0 "lin" 1 "1 GHz" 1 "2 GHz" 1 "501" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
</Components>
<Wires>
  <180 200 180 250 "" 0 0 0 "">
  <180 200 290 200 "" 0 0 0 "">
  <290 200 290 250 "" 0 0 0 "">
  <430 200 430 250 "" 0 0 0 "">
  <570 200 570 250 "" 0 0 0 "">
  <290 200 330 200 "" 0 0 0 "">
  <390 200 430 200 "" 0 0 0 "">
  <430 200 470 200 "" 0 0 0 "">
  <530 200 570 200 "" 0 0 0 "">
  <680 200 680 250 "" 0 0 0 "">
  <570 200 680 200 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Text 520 380 12 #000000 0 "Chebyshev low-pass filter \n 160mHz cutoff, pi-type, \n impedance matching 1 Ohm">
</Paintings>
