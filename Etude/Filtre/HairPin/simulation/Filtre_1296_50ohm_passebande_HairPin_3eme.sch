<Qucs Schematic 0.0.19>
<Properties>
  <View=-482,-266,1264,884,0.564474,0,0>
  <Grid=10,10,1>
  <DataSet=Filtre_1296_50ohm_passebande_HairPin_3eme.dat>
  <DataDisplay=Filtre_1296_50ohm_passebande_HairPin_3eme.dpl>
  <OpenDisplay=1>
  <Script=Filtre_1296_50ohm_passebande_HairPin_3eme.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Eqn Eqn1 1 -170 460 -31 15 0 0 "W1=1.5 m" 1 "S1=0.5 m" 1 "L1=Long" 1 "yes" 0>
  <Eqn Eqn2 1 -60 460 -31 15 0 0 "W2=1.5 m" 1 "S2=3m" 1 "L2=Long" 1 "yes" 0>
  <SUBST Sub1 1 540 490 -30 24 0 0 "4.5" 1 "800um" 1 "35um" 1 "0" 1 "1e-10" 1 "0" 1>
  <.SP SP1 1 710 470 0 67 0 0 "lin" 1 "1.15GHz" 1 "1.330GHz" 1 "401" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn7 1 950 470 -28 15 0 0 "S21_dB=dB(S[2,1])" 1 "S11_dB=dB(S[1,1])" 1 "yes" 0>
  <Eqn Eqn8 1 -270 450 -31 15 0 0 "W50=1.5 m" 1 "L50=var" 1 "yes" 0>
  <Eqn Eqn15 0 -270 570 -31 15 0 0 "Long=(31 m - (L50/2)) " 1 "yes" 0>
  <MCOUPLED MS1 1 20 90 37 -26 0 1 "Sub1" 1 "W1" 1 "L1" 1 "S1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS2 1 -10 10 -103 -12 1 1 "Sub1" 1 "W1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS3 1 180 220 15 -26 1 3 "Sub1" 1 "W2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCORN MS8 1 -10 220 -26 15 0 3 "Sub1" 1 "W1" 1>
  <GND * 1 -230 280 0 0 0 0>
  <Pac P1 1 -230 250 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <MLIN MS9 1 -110 220 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCORN MS10 1 50 -60 -7 -72 0 1 "Sub1" 1 "W1" 1>
  <MCORN MS11 1 180 -60 -26 -72 1 1 "Sub1" 1 "W2" 1>
  <MCORN MS13 1 240 280 -115 -26 0 2 "Sub1" 1 "W2" 1>
  <MLIN MS23 1 340 280 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCOUPLED MS18 1 670 160 37 -26 0 1 "Sub1" 1 "W6" 1 "L6" 1 "S6" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS19 1 640 240 15 -26 1 3 "Sub1" 1 "W6" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS20 1 470 150 -125 -26 0 3 "Sub1" 1 "W5" 1 "L5" 1 "S5" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS21 1 440 30 -112 -12 1 1 "Sub1" 1 "W5" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MLIN MS24 1 570 60 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCORN MS26 1 500 60 -7 -72 0 1 "Sub1" 1 "W5" 1>
  <MCORN MS30 1 640 60 -26 -72 1 1 "Sub1" 1 "W6" 1>
  <MOPEN MS31 1 700 -50 -112 -12 1 1 "Sub1" 1 "W6" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCORN MS34 1 440 240 -26 15 0 3 "Sub1" 1 "W5" 1>
  <MCORN MS35 1 700 300 -115 -26 0 2 "Sub1" 1 "W6" 1>
  <MLIN MS36 1 770 300 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P2 1 850 330 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 850 360 0 0 0 0>
  <MOPEN MS37 1 500 330 15 -26 1 3 "Sub1" 1 "W5" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS4 1 210 90 37 -26 0 1 "Sub1" 1 "W2" 1 "L2" 1 "S2" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS7 1 120 -60 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS5 1 240 -200 -112 -12 1 1 "Sub1" 1 "W2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS14 1 50 340 24 -26 1 3 "Sub1" 1 "W1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <Eqn Eqn5 1 70 460 -31 15 0 0 "W5=1.5 m" 1 "S5=3 m" 1 "L5=Long" 1 "yes" 0>
  <Eqn Eqn6 1 180 460 -31 15 0 0 "W6=1.5 m" 1 "S6=0.5 m" 1 "L6=Long" 1 "yes" 0>
  <Eqn Eqn16 1 -260 670 -31 15 0 0 "Long=28.5 m" 1 "yes" 0>
  <.SW SW1 1 190 650 0 61 0 0 "SP1" 1 "lin" 1 "var" 1 "1 m" 1 "5m" 1 "5" 1>
</Components>
<Wires>
  <-10 40 -10 60 "" 0 0 0 "">
  <-10 120 -10 190 "" 0 0 0 "">
  <-80 220 -40 220 "" 0 0 0 "">
  <-230 220 -140 220 "" 0 0 0 "">
  <50 -30 50 60 "" 0 0 0 "">
  <270 280 310 280 "" 0 0 0 "">
  <370 280 410 280 "" 0 0 0 "">
  <410 240 410 280 "" 0 0 0 "">
  <640 190 640 210 "" 0 0 0 "">
  <440 180 440 210 "" 0 0 0 "">
  <440 60 440 120 "" 0 0 0 "">
  <500 90 500 120 "" 0 0 0 "">
  <530 60 540 60 "" 0 0 0 "">
  <600 60 610 60 "" 0 0 0 "">
  <640 90 640 130 "" 0 0 0 "">
  <700 -20 700 130 "" 0 0 0 "">
  <700 190 700 270 "" 0 0 0 "">
  <730 300 740 300 "" 0 0 0 "">
  <800 300 850 300 "" 0 0 0 "">
  <500 180 500 300 "" 0 0 0 "">
  <240 120 240 250 "" 0 0 0 "">
  <180 -30 180 60 "" 0 0 0 "">
  <180 120 180 190 "" 0 0 0 "">
  <80 -60 90 -60 "" 0 0 0 "">
  <240 -170 240 60 "" 0 0 0 "">
  <50 120 50 310 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
