<Qucs Schematic 0.0.19>
<Properties>
  <View=-404,-76,1344,950,0.751315,5,0>
  <Grid=10,10,1>
  <DataSet=Filtre_1296_50ohm_passebande_HairPin.dat>
  <DataDisplay=Filtre_1296_50ohm_passebande_HairPin.dpl>
  <OpenDisplay=1>
  <Script=Filtre_1296_50ohm_passebande_HairPin.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Eqn Eqn7 1 -90 530 -31 15 0 0 "W1=1.45 m" 1 "S1=0.5 m" 1 "L1=Long" 1 "yes" 0>
  <Eqn Eqn6 1 20 530 -31 15 0 0 "W2=1.45 m" 1 "S2=3m" 1 "L2=Long" 1 "yes" 0>
  <Eqn Eqn5 1 130 530 -31 15 0 0 "W3=1.45 m" 1 "S3=4m" 1 "L3=Long" 1 "yes" 0>
  <Eqn Eqn4 1 240 530 -31 15 0 0 "W4=1.45 m" 1 "S4=4 m" 1 "L4=Long" 1 "yes" 0>
  <Eqn Eqn3 1 350 530 -31 15 0 0 "W5=1.45 m" 1 "S5=3 m" 1 "L5=Long" 1 "yes" 0>
  <Eqn Eqn2 1 460 530 -31 15 0 0 "W6=1.45 m" 1 "S6=0.5 m" 1 "L6=Long" 1 "yes" 0>
  <SUBST Sub1 1 620 560 -30 24 0 0 "4.5" 1 "800um" 1 "35um" 1 "0" 1 "1e-10" 1 "0" 1>
  <.SP SP1 1 790 540 0 67 0 0 "lin" 1 "1.15GHz" 1 "1.330GHz" 1 "401" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1030 540 -28 15 0 0 "S21_dB=dB(S[2,1])" 1 "S11_dB=dB(S[1,1])" 1 "yes" 0>
  <MCOUPLED MS1 1 -100 180 37 -26 0 1 "Sub1" 1 "W1" 1 "L1" 1 "S1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS3 1 -130 100 -103 -12 1 1 "Sub1" 1 "W1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS4 1 60 310 15 -26 1 3 "Sub1" 1 "W2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS17 1 90 180 37 -26 0 1 "Sub1" 1 "W2" 1 "L2" 1 "S2" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS18 1 120 80 -112 -12 1 1 "Sub1" 1 "W2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MLIN MS19 1 220 370 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Eqn Eqn14 1 -190 520 -31 15 0 0 "W50=1.45 m" 1 "L50=5 m" 1 "yes" 0>
  <Eqn Eqn13 0 -80 760 -31 15 0 0 "W1=1.45 m" 1 "S1=0.7 m" 1 "L1=31.1 m" 1 "yes" 0>
  <Eqn Eqn12 0 30 760 -31 15 0 0 "W2=1.45 m" 1 "S2=3.3 m" 1 "L2=31.1 m" 1 "yes" 0>
  <Eqn Eqn11 0 140 760 -31 15 0 0 "W3=1.45 m" 1 "S3=3.8 m" 1 "L3=31.1 m" 1 "yes" 0>
  <Eqn Eqn10 0 250 760 -31 15 0 0 "W4=1.45 m" 1 "S4=3.8 m" 1 "L4=31.1 m" 1 "yes" 0>
  <Eqn Eqn9 0 360 760 -31 15 0 0 "W5=1.45 m" 1 "S5=3.3 m" 1 "L5=31.1 m" 1 "yes" 0>
  <Eqn Eqn8 0 470 760 -31 15 0 0 "W6=1.45 m" 1 "S6=0.7 m" 1 "L6=31.1 m" 1 "yes" 0>
  <Eqn Eqn15 1 -190 640 -31 15 0 0 "Long=(31 m - (L50/2)) " 1 "yes" 0>
  <.SW SW1 1 680 740 0 60 0 0 "SP1" 1 "lin" 1 "VAR" 1 "0" 1 "10 m" 1 "11" 1>
  <MLIN MS23 1 0 30 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCORN MS25 1 -130 310 -26 15 0 3 "Sub1" 1 "W1" 1>
  <GND * 1 -350 370 0 0 0 0>
  <Pac P1 1 -350 340 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <MLIN MS24 1 -230 310 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCORN MS26 1 -70 30 -7 -72 0 1 "Sub1" 1 "W1" 1>
  <MCORN MS30 1 60 30 -26 -72 1 1 "Sub1" 1 "W2" 1>
  <MOPEN MS12 1 630 -40 -112 -12 1 1 "Sub1" 1 "W4" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCORN MS33 1 120 370 -115 -26 0 2 "Sub1" 1 "W2" 1>
  <MOPEN MS2 1 -30 430 24 -26 1 3 "Sub1" 1 "W1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS14 1 350 190 -125 -26 0 3 "Sub1" 1 "W3" 1 "L3" 1 "S3" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS16 1 380 410 15 -26 1 3 "Sub1" 1 "W3" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS15 1 320 80 -112 -12 1 1 "Sub1" 1 "W3" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS6 1 1030 210 37 -26 0 1 "Sub1" 1 "W6" 1 "L6" 1 "S6" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS8 1 1000 290 15 -26 1 3 "Sub1" 1 "W6" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS9 1 830 200 -125 -26 0 3 "Sub1" 1 "W5" 1 "L5" 1 "S5" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS5 1 800 80 -112 -12 1 1 "Sub1" 1 "W5" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS13 1 550 360 15 -26 1 3 "Sub1" 1 "W4" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MLIN MS20 1 700 290 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS22 1 930 110 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCORN MS27 1 380 110 -7 -72 0 1 "Sub1" 1 "W3" 1>
  <MCORN MS29 1 860 110 -7 -72 0 1 "Sub1" 1 "W5" 1>
  <MLIN MS21 1 450 110 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MCOUPLED MS11 1 580 180 37 -26 0 1 "Sub1" 1 "W4" 1 "L4" 1 "S4" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MCORN MS31 1 550 110 -26 -72 1 1 "Sub1" 1 "W4" 1>
  <MCORN MS32 1 1000 110 -26 -72 1 1 "Sub1" 1 "W6" 1>
  <MOPEN MS7 1 1060 0 -112 -12 1 1 "Sub1" 1 "W6" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCORN MS34 1 610 290 -115 -26 0 2 "Sub1" 1 "W4" 1>
  <MCORN MS35 1 320 370 -26 15 0 3 "Sub1" 1 "W3" 1>
  <MCORN MS36 1 800 290 -26 15 0 3 "Sub1" 1 "W5" 1>
  <MCORN MS37 1 1060 380 -115 -26 0 2 "Sub1" 1 "W4" 1>
  <MLIN MS38 1 1130 380 -26 15 0 0 "Sub1" 1 "W50" 1 "L50" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <GND * 1 1210 440 0 0 0 0>
  <Pac P2 1 1210 410 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <MOPEN MS10 1 860 420 15 -26 1 3 "Sub1" 1 "W5" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
</Components>
<Wires>
  <-130 130 -130 150 "" 0 0 0 "">
  <60 210 60 280 "" 0 0 0 "">
  <120 210 120 340 "" 0 0 0 "">
  <120 110 120 150 "" 0 0 0 "">
  <-130 210 -130 280 "" 0 0 0 "">
  <-200 310 -160 310 "" 0 0 0 "">
  <-350 310 -260 310 "" 0 0 0 "">
  <-70 60 -70 150 "" 0 0 0 "">
  <-40 30 -30 30 "" 0 0 0 "">
  <60 60 60 150 "" 0 0 0 "">
  <150 370 190 370 "" 0 0 0 "">
  <-70 210 -30 210 "" 0 0 0 "">
  <-30 210 -30 400 "" 0 0 0 "">
  <610 -10 630 -10 "" 0 0 0 "">
  <250 370 290 370 "" 0 0 0 "">
  <380 220 380 380 "" 0 0 0 "">
  <320 220 320 340 "" 0 0 0 "">
  <320 110 320 160 "" 0 0 0 "">
  <1000 240 1000 260 "" 0 0 0 "">
  <800 230 800 260 "" 0 0 0 "">
  <800 110 800 170 "" 0 0 0 "">
  <730 290 770 290 "" 0 0 0 "">
  <380 140 380 160 "" 0 0 0 "">
  <860 140 860 170 "" 0 0 0 "">
  <890 110 900 110 "" 0 0 0 "">
  <410 110 420 110 "" 0 0 0 "">
  <610 210 610 260 "" 0 0 0 "">
  <550 210 550 330 "" 0 0 0 "">
  <480 110 520 110 "" 0 0 0 "">
  <550 140 550 150 "" 0 0 0 "">
  <610 -10 610 150 "" 0 0 0 "">
  <960 110 970 110 "" 0 0 0 "">
  <1000 140 1000 180 "" 0 0 0 "">
  <1060 30 1060 180 "" 0 0 0 "">
  <640 290 670 290 "" 0 0 0 "">
  <1060 240 1060 350 "" 0 0 0 "">
  <1090 380 1100 380 "" 0 0 0 "">
  <1160 380 1210 380 "" 0 0 0 "">
  <860 230 860 390 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
