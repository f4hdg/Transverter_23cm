<Qucs Schematic 0.0.19>
<Properties>
  <View=-44,-219,1225,772,0.826446,0,120>
  <Grid=10,10,1>
  <DataSet=Filtre_1296_50ohm_passebande_ligne.dat>
  <DataDisplay=Filtre_1296_50ohm_passebande_ligne.dpl>
  <OpenDisplay=1>
  <Script=Filtre_1296_50ohm_passebande_ligne.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <MCOUPLED MS11 1 130 130 -26 37 0 0 "Sub1" 1 "W1" 1 "L1" 1 "S1" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS12 1 190 100 -26 -81 1 0 "Sub1" 1 "W1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <Pac P2 1 -10 300 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 -10 330 0 0 0 0>
  <MOPEN MS13 1 20 160 -12 15 1 2 "Sub1" 1 "W1" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS14 1 200 220 -12 15 1 2 "Sub1" 1 "W2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <Pac P1 1 1080 520 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 1080 550 0 0 0 0>
  <MOPEN MS8 1 920 340 -26 -81 1 0 "Sub1" 1 "W5" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS9 1 1010 430 -26 37 0 0 "Sub1" 1 "W6" 1 "L6" 1 "S6" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS10 1 1070 400 -26 -81 1 0 "Sub1" 1 "W6" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS18 1 900 460 -12 15 1 2 "Sub1" 1 "W6" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS7 1 830 370 -26 37 0 0 "Sub1" 1 "W5" 1 "L5" 1 "S5" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS17 1 730 400 -12 15 1 2 "Sub1" 1 "W5" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS5 1 660 310 -26 37 0 0 "Sub1" 1 "W4" 1 "L4" 1 "S4" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS6 1 720 280 -26 -81 1 0 "Sub1" 1 "W4" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS16 1 550 340 -12 15 1 2 "Sub1" 1 "W4" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS3 1 480 250 -26 37 0 0 "Sub1" 1 "W3" 1 "L3" 1 "S3" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS4 1 550 220 -26 -81 1 0 "Sub1" 1 "W3" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MOPEN MS15 1 380 280 -12 15 1 2 "Sub1" 1 "W3" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <MCOUPLED MS1 1 310 190 -26 37 0 0 "Sub1" 1 "W2" 1 "L2" 1 "S2" 1 "Kirschning" 0 "Kirschning" 0 "26.85" 0>
  <MOPEN MS2 1 380 160 -26 -81 1 0 "Sub1" 1 "W2" 1 "Hammerstad" 0 "Kirschning" 0 "Kirschning" 0>
  <SUBST Sub1 1 1070 90 -30 24 0 0 "R1" 1 "800um" 1 "35um" 1 "0" 1 "1e-10" 1 "0" 1>
  <Eqn Eqn1 1 590 40 -28 15 0 0 "S21_dB=dB(S[2,1])" 1 "S11_dB=dB(S[1,1])" 1 "yes" 0>
  <.SP SP1 1 780 30 0 67 0 0 "lin" 1 "1.25GHz" 1 "1.330GHz" 1 "401" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn13 1 580 520 -31 15 0 0 "W6=1.45 m" 1 "S6=0.5 m" 1 "L6=31 m" 1 "yes" 0>
  <Eqn Eqn12 1 470 520 -31 15 0 0 "W5=1.45 m" 1 "S5=3 m" 1 "L5=31 m" 1 "yes" 0>
  <Eqn Eqn11 1 360 520 -31 15 0 0 "W4=1.45 m" 1 "S4=4 m" 1 "L4=31 m" 1 "yes" 0>
  <Eqn Eqn10 1 250 520 -31 15 0 0 "W3=1.45 m" 1 "S3=4m" 1 "L3=31 m" 1 "yes" 0>
  <Eqn Eqn9 1 140 520 -31 15 0 0 "W2=1.45 m" 1 "S2=3m" 1 "L2=31 m" 1 "yes" 0>
  <Eqn Eqn8 1 30 520 -31 15 0 0 "W1=1.45 m" 1 "S1=0.5 m" 1 "L1=31 m" 1 "yes" 0>
  <Eqn Eqn14 0 590 660 -31 15 0 0 "W6=1.45 m" 1 "S6=0.7 m" 1 "L6=31.1 m" 1 "yes" 0>
  <Eqn Eqn15 0 480 660 -31 15 0 0 "W5=1.45 m" 1 "S5=3.3 m" 1 "L5=31.1 m" 1 "yes" 0>
  <Eqn Eqn16 0 370 660 -31 15 0 0 "W4=1.45 m" 1 "S4=3.8 m" 1 "L4=31.1 m" 1 "yes" 0>
  <Eqn Eqn17 0 260 660 -31 15 0 0 "W3=1.45 m" 1 "S3=3.8 m" 1 "L3=31.1 m" 1 "yes" 0>
  <Eqn Eqn18 0 150 660 -31 15 0 0 "W2=1.45 m" 1 "S2=3.3 m" 1 "L2=31.1 m" 1 "yes" 0>
  <Eqn Eqn19 0 40 660 -31 15 0 0 "W1=1.45 m" 1 "S1=0.7 m" 1 "L1=31.1 m" 1 "yes" 0>
  <.SW SW1 1 80 -190 0 58 0 0 "SP1" 1 "lin" 1 "R1" 1 "4.2" 1 "4.8" 1 "3" 1>
</Components>
<Wires>
  <-10 100 100 100 "" 0 0 0 "">
  <-10 100 -10 270 "" 0 0 0 "">
  <50 160 100 160 "" 0 0 0 "">
  <1080 460 1080 490 "" 0 0 0 "">
  <1040 460 1080 460 "" 0 0 0 "">
  <930 460 980 460 "" 0 0 0 "">
  <860 400 980 400 "" 0 0 0 "">
  <860 340 890 340 "" 0 0 0 "">
  <760 400 800 400 "" 0 0 0 "">
  <690 340 800 340 "" 0 0 0 "">
  <580 340 630 340 "" 0 0 0 "">
  <510 280 630 280 "" 0 0 0 "">
  <510 220 520 220 "" 0 0 0 "">
  <410 280 450 280 "" 0 0 0 "">
  <230 220 280 220 "" 0 0 0 "">
  <340 220 450 220 "" 0 0 0 "">
  <160 160 280 160 "" 0 0 0 "">
  <340 160 350 160 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
