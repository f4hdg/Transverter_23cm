EESchema Schematic File Version 2
LIBS:ade-25mh
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:sma
LIBS:ade-25mh-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ADE-25MH U1
U 1 1 5AA989A0
P 5550 3250
F 0 "U1" H 5550 3350 60  0000 C CNN
F 1 "ADE-25MH" H 5550 3500 60  0000 C CNN
F 2 "lib_kicad:PL-052" H 5550 3250 60  0001 C CNN
F 3 "" H 5550 3250 60  0001 C CNN
	1    5550 3250
	1    0    0    -1  
$EndComp
$Comp
L SMA J3
U 1 1 5AA989E6
P 6550 3550
F 0 "J3" H 6675 3865 60  0000 C CNN
F 1 "SMA" H 6740 3790 60  0000 C CNN
F 2 "lib_kicad:SMA" H 6725 3720 50  0000 C CNN
F 3 "" H 6550 3550 60  0001 C CNN
	1    6550 3550
	1    0    0    -1  
$EndComp
$Comp
L SMA J1
U 1 1 5AA98A29
P 4850 3550
F 0 "J1" H 4975 3865 60  0000 C CNN
F 1 "SMA" H 5040 3790 60  0000 C CNN
F 2 "lib_kicad:SMA" H 5025 3720 50  0000 C CNN
F 3 "" H 4850 3550 60  0001 C CNN
	1    4850 3550
	-1   0    0    -1  
$EndComp
$Comp
L SMA J2
U 1 1 5AA98A52
P 5250 3900
F 0 "J2" H 5375 4215 60  0000 C CNN
F 1 "SMA" H 5440 4140 60  0000 C CNN
F 2 "lib_kicad:SMA" H 5425 4070 50  0000 C CNN
F 3 "" H 5250 3900 60  0001 C CNN
	1    5250 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 3250 5150 3250
Wire Wire Line
	4800 3450 4950 3450
Connection ~ 4850 3450
Connection ~ 4900 3450
Wire Wire Line
	5800 3700 6000 3700
Connection ~ 5900 3700
Wire Wire Line
	5550 3750 5550 3650
Wire Wire Line
	5950 3250 6400 3250
Wire Wire Line
	6450 3450 6600 3450
Connection ~ 6550 3450
Connection ~ 6500 3450
Wire Wire Line
	5350 3800 5350 4250
Connection ~ 5350 3850
Connection ~ 5350 3900
Connection ~ 5350 3950
$Comp
L GND #PWR01
U 1 1 5AA98BEE
P 5350 4250
F 0 "#PWR01" H 5350 4000 50  0001 C CNN
F 1 "GND" H 5350 4100 50  0000 C CNN
F 2 "" H 5350 4250 50  0001 C CNN
F 3 "" H 5350 4250 50  0001 C CNN
	1    5350 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5AA98C0A
P 4900 3550
F 0 "#PWR02" H 4900 3300 50  0001 C CNN
F 1 "GND" H 4900 3400 50  0000 C CNN
F 2 "" H 4900 3550 50  0001 C CNN
F 3 "" H 4900 3550 50  0001 C CNN
	1    4900 3550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5AA98C26
P 6550 3550
F 0 "#PWR03" H 6550 3300 50  0001 C CNN
F 1 "GND" H 6550 3400 50  0000 C CNN
F 2 "" H 6550 3550 50  0001 C CNN
F 3 "" H 6550 3550 50  0001 C CNN
	1    6550 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3550 6550 3450
Wire Wire Line
	4900 3550 4900 3450
$Comp
L GND #PWR04
U 1 1 5AA98C6D
P 5900 3850
F 0 "#PWR04" H 5900 3600 50  0001 C CNN
F 1 "GND" H 5900 3700 50  0000 C CNN
F 2 "" H 5900 3850 50  0001 C CNN
F 3 "" H 5900 3850 50  0001 C CNN
	1    5900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3850 5900 3700
$EndSCHEMATC
