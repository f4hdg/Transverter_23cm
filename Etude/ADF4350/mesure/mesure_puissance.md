# Mesure du 27 mai 2018

## Au Puissance metre
### Mesure direct 
* -5,45 dBm  @1152 MHz connecteur SMA(m)-SMA(m)
* -6,54 dBm  @1152 MHz cordon douche SMA(m)-SMA(m)
* -7,86 dBm  @1152 MHz cordon noir SMA(m)-SMA(m)

* Perte du cable
  * noir   -2,42 dB
  * douche -1.1  dB

### Mesure avec ERA-5
5,84 dBm @1152 MHz
S21 ERA-5 = 13,7dB

## Au Spectrum

