// This Lib is mix of :
// Integer mode Demo OE6OCG 1/2015 finded on KD0CQ website
// JQIamo : https://github.com/JQIamo/ADF4350-arduino/
//
//*****************************************************************
// LibADF4350 PLL-Synthesizer
//*****************************************************************
/*                                               Hardware connection Uno to PLL-Board (3.3V logic)
                                  +-----+
     +----[PWR]-------------------| USB |--+
     |                            +-----+  |                        output: 33Mhz - 4.4Ghz ~ 3 dbm
     |         GND/RST2  [ ][ ]            |             PLL-Board + Arduino with Display Bk.light draw 200mA on 5V USB 
     |       MOSI2/SCK2  [ ][ ]  A5/SCL[ ] |                                       #
     |          5V/MISO2 [ ][ ]  A4/SDA[ ] |    |---|_3.3k_|-----\                 |
     |                             AREF[ ] |    !---|_3.3k_|---\ |            _____|______
     |                              GND[ ] |----!---|_3.3k_|-| | |           |            |
     | [ ]N/C                    SCK/13[ ] |------|_1.5k_|---!-|-|----Clock--|ADF4351     |
     | [ ]v.ref                 MISO/12[ ] |                   | |           |PLL Board   |
     | [ ]RST                   MOSI/11[ ]~|------|_1.5k_|-----!-|----Data---|3.3V logic  |
     | [ ]3V3    +---+               10[ ]~|                     |           |            |
     | [ ]5v     | A |                9[ ]~|   LCD               !----LE-----|            |
     | [ ]GND   -| R |-               8[ ] |   LCD               |           |____________|
     | [ ]GND   -| D |-                    |                     |
     | [ ]Vin   -| U |-               7[ ] |   LCD               |
     |          -| I |-               6[ ]~|   LCD               |
Keyb | [ ]A0    -| N |-               5[ ]~|   LCD               |
RSSI | [ ]A1    -| O |-               4[ ] |   LCD               |
     | [ ]A2     +---+           INT1/3[ ]~|------|_1.5k_|---LE--/
     | [ ]A3                     INT0/2[ ] |
     | [ ]A4/SDA  RST SCK MISO     TX>1[ ] |
     | [ ]A5/SCL  [ ] [ ] [ ]      RX<0[ ] |
     |            [ ] [ ] [ ]              |
     |  UNO       GND MOSI 5V  ____________/
      \_______________________/

*/

#ifndef LibADF4350_h
#define LibADF4350_h

#include "Arduino.h"

class ADF4350
{
    public: 
        // Constructor function. 
        // Creates PLL object, with given SS pin
        ADF4350(int);

        // Initialize with initial frequency, refClk (defaults to 10Mhz); 
        void initialize(unsigned long, long);

        // powers down the PLL/VCO
        void powerDown(bool);
        void setRfPower(int);
        void setAuxPower(int);
        void auxEnable(bool);
        void rfEnable(bool);

        // Gets current frequency
        unsigned long getFreq();
        long getChan();
        void setChan(long Chan);

        // Sets frequency
        void setFreq(unsigned long);
        
        void setFeedbackType(bool);

        void send();

    private:
        int slaveSelectPin;  //SPI-SS bzw. enable ADF4350
        unsigned long Freq;  //Startfrequenz generel 100Hz aulösung
        long refin; // Refrenquarz = 25Mhz
        long ChanStep;  //625; //Kanalraster = 6,25Khz kleinstes raster
        unsigned long Reg[6]; //ADF4351 Reg's
        
///////////////////////////////////////////////////////////    REGISTRE 
        // PLL-Reg-R0         =  32bit
        // Registerselect        3bit
        // int F_Frac = 4;       // 12bit
        // int N_Int = 92;       // 16bit
        // reserved           // 1bit
        
        // PLL-Reg-R1         =  32bit
        // Registerselect        3bit
        //int M_Mod = 5;        // 12bit
        int P_Phase;     // 12bit bei 2x12bit hintereinander pow()-bug !!
        int Prescal;     // 1bit geht nicht ???
        int PhaseAdj;    // 1bit geht auch nicht ???
        // reserved           // 3bit
        
        // PLL-Reg-R2         =  32bit
        // Registerselect        3bit
        int U1_CountRes; // 1bit
        int U2_Cp3state; // 1bit
        int U3_PwrDown;  // 1bit
        int U4_PDpola;    // 1bit
        int U5_LPD;       // 1bit
        int U6_LPF;       // 1bit 1=Integer, 0=Frac not spported yet
        int CP_ChgPump;     // 4bit
        int D1_DoublBuf; // 1bit
        //  int R_Counter = 1;   // 10bit
        //  int RD1_Rdiv2 = 0;    // 1bit
        //  int RD2refdoubl = 0; // 1bit
        int M_Muxout;     // 3bit
        int LoNoisSpur;      // 2bit
        // reserved           // 1bit
        
        // PLL-Reg-R3         =  32bit
        // Registerselect        3bit
        int D_Clk_div; // 12bit
        int C_Clk_mode;   // 2bit
        //  reserved          // 1bit
        int F1_Csr;       // 1bit
        //  reserved          // 2bit
        int F2_ChgChan;   // 1bit
        int F3_ADB;       // 1bit
        int F4_BandSel;  // 1bit
        //  reserved          // 8bit

        // PLL-Reg-R4         =  32bit
        // Registerselect        3bit
        int D_out_PWR;    // 2bit
        int D_RF_ena;     // 1bit
        int D_auxOutPwr;  // 2bit
        int D_auxOutEna;  // 1bit
        int D_auxOutSel;  // 1bit
        int D_MTLD;       // 1bit
        int D_VcoPwrDown; // 1bit 1=VCO off


        //  int B_BandSelClk = 200; // 8bit

        int D_RfDivSel;    // 3bit 3=70cm 4=2m
        int D_FeedBck;     // 1bit
        // reserved           // 8bit
      
        // PLL-Reg-R5         =  32bit
        // Registerselect     // 3bit
        // reserved           // 16bit
        // reserved     11    // 2bit
        // reserved           // 1bit
        int D_LdPinMod;      // 2bit muss 1 sein
        // reserved           // 8bit

        // Referenz Freg Calc
        //  long refin = 250000; // Refrenquarz = 25000000hz
        int R_Counter;   // 10bit
        int RD1_Rdiv2;    // 1bit
        int RD2refdoubl; // 1bit
        int B_BandSelClk; // 8bit
        //  int F4_BandSel = 0;  // 1bit

        // int F4_BandSel = 10.0 * B_BandSelClk / PFDFreq;

        
        void setRegister();
        void WriteADF2(int);
        int WriteADF(byte, byte, byte, byte);
        int Toggle();
};

#endif
