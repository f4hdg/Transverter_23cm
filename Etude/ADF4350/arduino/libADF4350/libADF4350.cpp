// This Lib is mix of :
// Integer mode Demo OE6OCG 1/2015 finded on KD0CQ website
// JQIamo : https://github.com/JQIamo/ADF4350-arduino/
//
//*****************************************************************
// LibADF4350 PLL-Synthesizer
//*****************************************************************
/*                                               Hardware connection Uno to PLL-Board (3.3V logic)
                                  +-----+
     +----[PWR]-------------------| USB |--+
     |                            +-----+  |                        output: 33Mhz - 4.4Ghz ~ 3 dbm
     |         GND/RST2  [ ][ ]            |             PLL-Board + Arduino with Display Bk.light draw 200mA on 5V USB 
     |       MOSI2/SCK2  [ ][ ]  A5/SCL[ ] |                                       #
     |          5V/MISO2 [ ][ ]  A4/SDA[ ] |    |---|_3.3k_|-----\                 |
     |                             AREF[ ] |    !---|_3.3k_|---\ |            _____|______
     |                              GND[ ] |----!---|_3.3k_|-| | |           |            |
     | [ ]N/C                    SCK/13[ ] |------|_1.5k_|---!-|-|----Clock--|ADF4351     |
     | [ ]v.ref                 MISO/12[ ] |                   | |           |PLL Board   |
     | [ ]RST                   MOSI/11[ ]~|------|_1.5k_|-----!-|----Data---|3.3V logic  |
     | [ ]3V3    +---+               10[ ]~|                     |           |            |
     | [ ]5v     | A |                9[ ]~|   LCD               !----LE-----|            |
     | [ ]GND   -| R |-               8[ ] |   LCD               |           |____________|
     | [ ]GND   -| D |-                    |                     |
     | [ ]Vin   -| U |-               7[ ] |   LCD               |
     |          -| I |-               6[ ]~|   LCD               |
Keyb | [ ]A0    -| N |-               5[ ]~|   LCD               |
RSSI | [ ]A1    -| O |-               4[ ] |   LCD               |
     | [ ]A2     +---+           INT1/3[ ]~|------|_1.5k_|---LE--/
     | [ ]A3                     INT0/2[ ] |
     | [ ]A4/SDA  RST SCK MISO     TX>1[ ] |
     | [ ]A5/SCL  [ ] [ ] [ ]      RX<0[ ] |
     |            [ ] [ ] [ ]              |
     |  UNO       GND MOSI 5V  ____________/
      \_______________________/

*/

#include "Arduino.h"
#include "SPI.h"
#include <libADF4350.h>

/* CONSTRUCTOR */

// Constructor function; initializes communication pinouts

ADF4350::ADF4350(int ssPin) {
    slaveSelectPin = ssPin;
    pinMode (slaveSelectPin, OUTPUT);
    digitalWrite(slaveSelectPin, LOW);
    //delay(10);
}


// Initializes a new ADF4350 object, with refClk (in Mhz), and initial frequency.
void ADF4350::initialize(unsigned long freq, long refClk){
    refin = refClk/10;
    P_Phase = 1;
    Prescal = 0;
    PhaseAdj = 0;
    U1_CountRes = 0;
    U2_Cp3state = 0;
    U3_PwrDown = 0;
    U4_PDpola = 1;
    U5_LPD = 0;
    U6_LPF = 1;
    CP_ChgPump = 7;
    D1_DoublBuf = 0;
    M_Muxout = 0;
    LoNoisSpur = 0;
    D_Clk_div = 150;
    C_Clk_mode = 0;
    F1_Csr = 0;
    F2_ChgChan = 0;
    F3_ADB = 0;
    F4_BandSel = 0;
    D_out_PWR = 0 ;
    D_RF_ena = 1;
    D_auxOutPwr = 0;
    D_auxOutEna = 0;
    D_auxOutSel = 0;
    D_MTLD = 0;
    D_VcoPwrDown = 0;
    D_RfDivSel = 3;
    D_FeedBck = 1;
    D_LdPinMod = 1;
    R_Counter = 1;
    RD1_Rdiv2 = 0;
    RD2refdoubl = 0;
    B_BandSelClk = 200;
    ChanStep = 1;// 625 origine //Kanalraster = 6,25Khz kleinstes raster
    //Freq = 15000000;  //Startfrequenz generel 100Hz aulösung
    
    Freq = freq/10;
    ADF4350::setRegister();
    ADF4350::send();

}

// gets current frequency setting
unsigned long ADF4350::getFreq(){
    return Freq*10;
}

void ADF4350::setFreq(unsigned long freq){
    Freq = freq/10;
    ADF4350::setRegister();
    ADF4350::send();
}

// gets current frequency setting
long ADF4350::getChan(){
    return ChanStep*10;
}

void ADF4350::setChan(long Chan){
    ChanStep = Chan/10;
    ADF4350::setRegister();
    ADF4350::send();
}

void ADF4350::setFeedbackType(bool feedback){
    D_FeedBck = feedback;
    ADF4350::setRegister();
    ADF4350::send();
}

void ADF4350::powerDown(bool pd){
    U3_PwrDown = pd;
    ADF4350::setRegister();
    ADF4350::send();
}

void ADF4350::rfEnable(bool rf){
    D_RF_ena = rf;
    ADF4350::setRegister();
    ADF4350::send();
}

// CAREFUL!!!! pow must be 0, 1, 2, or 3... corresponding to -4, -1, 3, 5 dbm.
void ADF4350::setRfPower(int pow){
    if(pow < 0)
    {
        pow = 0;
    }
    else if(pow > 3)
    {
        pow = 3;
    }
    D_out_PWR = pow;
    ADF4350::setRegister();
    ADF4350::send();
}

void ADF4350::auxEnable(bool aux){
    D_auxOutEna = aux;
    ADF4350::setRegister();
    ADF4350::send();
}

// CAREFUL!!!! pow must be 0, 1, 2, or 3... corresponding to -4, -1, 3, 5 dbm.
void ADF4350::setAuxPower(int pow){
        if(pow < 0)
    {
        pow = 0;
    }
    else if(pow > 3)
    {
        pow = 3;
    }
    D_auxOutPwr = pow;
    ADF4350::setRegister();
    ADF4350::send();
}

// updates dynamic registers, and writes values to PLL board
void ADF4350::send(){
     ADF4350::setRegister();
     ADF4350::WriteADF2(5);
     delayMicroseconds(2500);
     ADF4350::WriteADF2(4);
     delayMicroseconds(2500);
     ADF4350::WriteADF2(3);
     delayMicroseconds(2500);
     ADF4350::WriteADF2(2);
     delayMicroseconds(2500);
     ADF4350::WriteADF2(1);
     delayMicroseconds(2500);
     ADF4350::WriteADF2(0);
     delayMicroseconds(2500);
}

//////////////////////////////////////////// REGISTER UPDATE FUNCTIONS
void ADF4350::setRegister(){
  long RFout = Freq;   // VCO-Frequenz
  // calc bandselect und RF-div
  int outdiv = 1;

  if (RFout >= 220000000) {
    outdiv = 1;
    D_RfDivSel = 0;
  }
  if (RFout < 220000000) {
    outdiv = 2;
    D_RfDivSel = 1;
  }
  if (RFout < 110000000) {
    outdiv = 4;
    D_RfDivSel = 2;
  }
  if (RFout < 55000000) {
    outdiv = 8;
    D_RfDivSel = 3;
  }
  if (RFout < 27500000) {
    outdiv = 16;
    D_RfDivSel = 4;
  }
  if (RFout < 13800000) {
    outdiv = 32;
    D_RfDivSel = 5;
  }
  if (RFout < 6900000) {
    outdiv = 64;
    D_RfDivSel = 6;
  }

  float PFDFreq = refin * ((1.0 + RD2refdoubl) / (R_Counter * (1.0 + RD1_Rdiv2))); //Referenzfrequenz
  float N = ((RFout) * outdiv) / PFDFreq;
  int N_Int = N;
  long M_Mod = PFDFreq * (100000 / ChanStep) / 100000;
  int F_Frac = round((N - N_Int) * M_Mod);

  Reg[0] = (unsigned long)(0 + F_Frac * pow(2, 3) + N_Int * pow(2, 15));
  Reg[1] = (unsigned long)(1 + M_Mod * pow(2, 3) + P_Phase * pow(2, 15) + Prescal * pow(2, 27) + PhaseAdj * pow(2, 28));
  //  R[1] = (R[1])+1; // Registerselect adjust ?? because unpossible 2x12bit in pow() funktion
  Reg[2] = (unsigned long)(2 + U1_CountRes * pow(2, 3) + U2_Cp3state * pow(2, 4) + U3_PwrDown * pow(2, 5) + U4_PDpola * pow(2, 6) + U5_LPD * pow(2, 7) + U6_LPF * pow(2, 8) + CP_ChgPump * pow(2, 9) + D1_DoublBuf * pow(2, 13) + R_Counter * pow(2, 14) + RD1_Rdiv2 * pow(2, 24) + RD2refdoubl * pow(2, 25) + M_Muxout * pow(2, 26) + LoNoisSpur * pow(2, 29));
  Reg[3] = (unsigned long)(3 + D_Clk_div * pow(2, 3) + C_Clk_mode * pow(2, 15) + 0 * pow(2, 17) + F1_Csr * pow(2, 18) + 0 * pow(2, 19) + F2_ChgChan * pow(2, 21) +  F3_ADB * pow(2, 22) + F4_BandSel * pow(2, 23) + 0 * pow(2, 24));
  Reg[4] = (unsigned long)(4 + D_out_PWR * pow(2, 3) + D_RF_ena * pow(2, 5) + D_auxOutPwr * pow(2, 6) + D_auxOutEna * pow(2, 8) + D_auxOutSel * pow(2, 9) + D_MTLD * pow(2, 10) + D_VcoPwrDown * pow(2, 11) + B_BandSelClk * pow(2, 12) + D_RfDivSel * pow(2, 20) + D_FeedBck * pow(2, 23));
  Reg[5] = (unsigned long)(5 + 0 * pow(2, 3) + 3 * pow(2, 19) + 0 * pow(2, 21) + D_LdPinMod * pow(2, 22));
}
/////////////////////////////////////////////////////////////

void ADF4350::WriteADF2(int idx)
{ // make 4 byte from integer for SPI-Transfer
    byte buf[4];
    for (int i = 0; i < 4; i++)
        buf[i] = (byte)(Reg[idx] >> (i * 8));
    WriteADF(buf[3], buf[2], buf[1], buf[0]);
}
int ADF4350::WriteADF(byte a1, byte a2, byte a3, byte a4) {
    // write over SPI to ADF4350
    digitalWrite(slaveSelectPin, LOW);
    delayMicroseconds(10);
    SPI.transfer(a1);
    SPI.transfer(a2);
    SPI.transfer(a3);
    SPI.transfer(a4);
    Toggle();
}
int ADF4350::Toggle() {
    digitalWrite(slaveSelectPin, HIGH);
    delayMicroseconds(5);
    digitalWrite(slaveSelectPin, LOW);
}

