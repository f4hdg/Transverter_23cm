<Qucs Schematic 0.0.19>
<Properties>
  <View=-94,-174,1360,722,1,45,260>
  <Grid=10,10,1>
  <DataSet=SPF5189Z.dat>
  <DataDisplay=SPF5189Z.dpl>
  <OpenDisplay=1>
  <Script=SPF5189Z.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SPTC1 1 90 490 0 59 0 0 "log" 1 "5 MHz" 1 "10 GHz" 1 "1000" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <SUBST SubstTC1 1 540 -110 -30 24 0 0 "4.6" 1 "1.6 mm" 1 "35 um" 1 "0" 1 "2.43902e-08" 1 "0" 1>
  <GND * 1 380 350 0 0 0 0>
  <MLIN MSTC1 1 160 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MSTC2 2 310 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "3 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <L L1 2 250 320 -26 10 0 0 "var" 1 "" 0>
  <Eqn Eqn1 1 730 160 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S22dB=dB(S[2,2])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <MLIN MSTC3 1 460 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MSTC4 2 560 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <L L2 1 510 210 10 -26 0 1 "var" 1 "" 0>
  <Vdc V1 1 410 170 18 -26 0 1 "1 V" 1>
  <GND * 1 410 200 0 0 0 0>
  <Pac P2 1 810 350 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 810 380 0 0 0 0>
  <Pac P1 1 -10 350 -74 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 -10 380 0 0 0 0>
  <MLIN MSTC5 1 20 320 -19 24 0 0 "SubstTC1" 1 "3 mm" 1 "10 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MSTC6 1 720 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "10mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <.SW SW2 0 370 510 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "1 mm" 1 "10 mm" 1 "5" 1>
  <.SW SW3 0 600 510 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "1 p" 1 "100 p" 1 "5" 1>
  <C C1 1 90 320 -24 -55 0 0 "100 p" 1 "" 0 "neutral" 0>
  <C C2 1 630 320 -22 -56 0 0 "100 p" 1 "" 0 "neutral" 0>
  <.SW SW4 1 770 500 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "0" 1 "50 n" 1 "10" 1>
  <SPfile X1 1 380 320 -31 -97 0 0 "/home/nop/Radioamateur/git/Transverter_23cm/SPF5189Z/doc/SPF-5189Z_Deembedded.s2p" 1 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
</Components>
<Wires>
  <410 320 430 320 "" 0 0 0 "">
  <190 320 220 320 "" 0 0 0 "">
  <340 320 350 320 "" 0 0 0 "">
  <490 320 510 320 "" 0 0 0 "">
  <510 320 530 320 "" 0 0 0 "">
  <510 240 510 320 "" 0 0 0 "">
  <410 130 410 140 "" 0 0 0 "">
  <410 130 510 130 "" 0 0 0 "">
  <510 130 510 180 "" 0 0 0 "">
  <120 320 130 320 "" 0 0 0 "">
  <590 320 600 320 "" 0 0 0 "">
  <660 320 690 320 "" 0 0 0 "">
  <50 320 60 320 "" 0 0 0 "">
  <750 320 810 320 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
