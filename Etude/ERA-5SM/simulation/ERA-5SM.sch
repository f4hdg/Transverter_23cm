<Qucs Schematic 0.0.19>
<Properties>
  <View=-44,-194,1253,800,1,44,194>
  <Grid=10,10,1>
  <DataSet=ERA-5SM.dat>
  <DataDisplay=ERA-5SM.dpl>
  <OpenDisplay=1>
  <Script=ERA-5SM.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titre>
  <FrameText1=Auteur :>
  <FrameText2=Date :>
  <FrameText3=Version :>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SPTC1 1 170 490 0 59 0 0 "log" 1 "500 MHz" 1 "2.5 GHz" 1 "100" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <SUBST SubstTC1 1 620 -110 -30 24 0 0 "4.6" 1 "1.6 mm" 1 "35 um" 1 "0" 1 "2.43902e-08" 1 "0" 1>
  <GND * 1 460 350 0 0 0 0>
  <MLIN MSTC1 1 240 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "5 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MSTC2 2 390 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <L L1 2 330 320 -26 10 0 0 "1 n" 1 "" 0>
  <Eqn Eqn1 1 810 160 -31 15 0 0 "S11dB=dB(S[1,1])" 1 "S22dB=dB(S[2,2])" 1 "S21dB=dB(S[2,1])" 1 "yes" 0>
  <MLIN MSTC3 1 540 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MSTC4 2 640 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "2 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <L L2 1 590 210 10 -26 0 1 "150 nH" 1 "" 0>
  <Vdc V1 1 490 170 18 -26 0 1 "1 V" 1>
  <GND * 1 490 200 0 0 0 0>
  <Pac P1 1 890 350 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 890 380 0 0 0 0>
  <Pac P2 1 70 350 -74 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 70 380 0 0 0 0>
  <MLIN MSTC5 1 100 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "10 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MSTC6 1 800 320 -26 15 0 0 "SubstTC1" 1 "3 mm" 1 "10mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <.SW SW1 0 450 510 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "1 mm" 1 "10 mm" 1 "5" 1>
  <.SW SW2 1 680 510 0 59 0 0 "SPTC1" 1 "lin" 1 "var" 1 "1 p" 1 "100 p" 1 "5" 1>
  <C C1 1 170 320 -26 17 0 0 "100 pF" 1 "" 0 "neutral" 0>
  <C C2 1 710 320 -26 17 0 0 "100 pF" 1 "" 0 "neutral" 0>
  <SPfile X1 1 460 320 -26 -59 0 0 "/home/nop/Radioamateur/git/Transverter_23cm/ERA-5SM/doc/ERA-5SM+___65mA___Plus25degC.S2P" 1 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
</Components>
<Wires>
  <490 320 510 320 "" 0 0 0 "">
  <270 320 300 320 "" 0 0 0 "">
  <420 320 430 320 "" 0 0 0 "">
  <570 320 590 320 "" 0 0 0 "">
  <590 320 610 320 "" 0 0 0 "">
  <590 240 590 320 "" 0 0 0 "">
  <490 130 490 140 "" 0 0 0 "">
  <490 130 590 130 "" 0 0 0 "">
  <590 130 590 180 "" 0 0 0 "">
  <200 320 210 320 "" 0 0 0 "">
  <670 320 680 320 "" 0 0 0 "">
  <740 320 770 320 "" 0 0 0 "">
  <130 320 140 320 "" 0 0 0 "">
  <830 320 890 320 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
