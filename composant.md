# Composant Transverter

## Regulateur
 - 8V   : 7808 SOT89
 - 5V   : AMS1117 SOT223
 - 3.3V : AMS1117 SOT223

## Amplificateur
 - IF  : ERA-5M
 - LNA : SPF5189Z SOT89

## Melangeur
-  Melangeur : ADE-25MH+

## Interrupteur
 - RF integré : AS179-92LFT
 - Relais     : TQ2-5V

## Boitier
 - 108 x 35 mm
 - 71  x 35 mm
 - 54  x 54 mm
 - 72  x 72 mm

 
