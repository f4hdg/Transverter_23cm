EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PTT-rescue:HRS2H-S-HRS2H-S U20
U 1 1 5CD8CE7B
P 6900 4650
F 0 "U20" H 7228 4696 50  0000 L CNN
F 1 "HRS2H-S" H 7228 4605 50  0000 L CNN
F 2 "Relay_THT:Relay_DPDT_Omron_G5V-2" H 6900 4650 50  0001 C CNN
F 3 "" H 6900 4650 50  0001 C CNN
	1    6900 4650
	1    0    0    -1  
$EndComp
$Comp
L power:+8V #PWR0102
U 1 1 5CD8F393
P 6200 4300
F 0 "#PWR0102" H 6200 4150 50  0001 C CNN
F 1 "+8V" H 6215 4473 50  0000 C CNN
F 2 "" H 6200 4300 50  0001 C CNN
F 3 "" H 6200 4300 50  0001 C CNN
	1    6200 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4400 6350 4400
$Comp
L power:GND #PWR0103
U 1 1 5CD911BD
P 2250 2950
F 0 "#PWR0103" H 2250 2700 50  0001 C CNN
F 1 "GND" H 2255 2777 50  0000 C CNN
F 2 "" H 2250 2950 50  0001 C CNN
F 3 "" H 2250 2950 50  0001 C CNN
	1    2250 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5CD91296
P 3300 2950
F 0 "#PWR0104" H 3300 2700 50  0001 C CNN
F 1 "GND" H 3305 2777 50  0000 C CNN
F 2 "" H 3300 2950 50  0001 C CNN
F 3 "" H 3300 2950 50  0001 C CNN
	1    3300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 2950 2250 2900
Wire Wire Line
	3300 2950 3300 2900
Wire Wire Line
	1950 2600 1650 2600
Wire Wire Line
	3600 2600 4050 2600
$Comp
L power:+5V #PWR0105
U 1 1 5CD92125
P 4050 2500
F 0 "#PWR0105" H 4050 2350 50  0001 C CNN
F 1 "+5V" H 4065 2673 50  0000 C CNN
F 2 "" H 4050 2500 50  0001 C CNN
F 3 "" H 4050 2500 50  0001 C CNN
	1    4050 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+8V #PWR0106
U 1 1 5CD92677
P 2750 2500
F 0 "#PWR0106" H 2750 2350 50  0001 C CNN
F 1 "+8V" H 2765 2673 50  0000 C CNN
F 2 "" H 2750 2500 50  0001 C CNN
F 3 "" H 2750 2500 50  0001 C CNN
	1    2750 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0107
U 1 1 5CD92AE8
P 1300 2500
F 0 "#PWR0107" H 1300 2350 50  0001 C CNN
F 1 "+12V" H 1315 2673 50  0000 C CNN
F 2 "" H 1300 2500 50  0001 C CNN
F 3 "" H 1300 2500 50  0001 C CNN
	1    1300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 2500 4050 2600
Wire Wire Line
	2550 2600 2750 2600
Wire Wire Line
	2750 2500 2750 2600
Connection ~ 2750 2600
Wire Wire Line
	2750 2600 3000 2600
Wire Wire Line
	1300 2500 1300 2600
$Comp
L Device:C C10
U 1 1 5CD93E0F
P 1650 2750
F 0 "C10" H 1765 2796 50  0000 L CNN
F 1 "1u" H 1765 2705 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D5.0mm_H5.0mm_P2.00mm" H 1688 2600 50  0001 C CNN
F 3 "~" H 1650 2750 50  0001 C CNN
	1    1650 2750
	1    0    0    -1  
$EndComp
Connection ~ 1650 2600
Wire Wire Line
	1650 2600 1300 2600
$Comp
L Device:C C11
U 1 1 5CD94034
P 2750 2750
F 0 "C11" H 2865 2796 50  0000 L CNN
F 1 "1u" H 2865 2705 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D5.0mm_H5.0mm_P2.00mm" H 2788 2600 50  0001 C CNN
F 3 "~" H 2750 2750 50  0001 C CNN
	1    2750 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5CD94364
P 4050 2750
F 0 "C12" H 4165 2796 50  0000 L CNN
F 1 "1u" H 4165 2705 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D5.0mm_H5.0mm_P2.00mm" H 4088 2600 50  0001 C CNN
F 3 "~" H 4050 2750 50  0001 C CNN
	1    4050 2750
	1    0    0    -1  
$EndComp
Connection ~ 4050 2600
$Comp
L power:GND #PWR0108
U 1 1 5CD945DC
P 1650 2900
F 0 "#PWR0108" H 1650 2650 50  0001 C CNN
F 1 "GND" H 1655 2727 50  0000 C CNN
F 2 "" H 1650 2900 50  0001 C CNN
F 3 "" H 1650 2900 50  0001 C CNN
	1    1650 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5CD9475A
P 2750 2900
F 0 "#PWR0109" H 2750 2650 50  0001 C CNN
F 1 "GND" H 2755 2727 50  0000 C CNN
F 2 "" H 2750 2900 50  0001 C CNN
F 3 "" H 2750 2900 50  0001 C CNN
	1    2750 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5CD949E8
P 4050 2900
F 0 "#PWR0110" H 4050 2650 50  0001 C CNN
F 1 "GND" H 4055 2727 50  0000 C CNN
F 2 "" H 4050 2900 50  0001 C CNN
F 3 "" H 4050 2900 50  0001 C CNN
	1    4050 2900
	1    0    0    -1  
$EndComp
Text GLabel 6900 3700 1    50   Input ~ 0
COM_TRX
$Comp
L power:+5V #PWR0111
U 1 1 5CD9A014
P 7100 4050
F 0 "#PWR0111" H 7100 3900 50  0001 C CNN
F 1 "+5V" H 7115 4223 50  0000 C CNN
F 2 "" H 7100 4050 50  0001 C CNN
F 3 "" H 7100 4050 50  0001 C CNN
	1    7100 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5CD9A3F7
P 7000 3900
F 0 "#PWR0112" H 7000 3650 50  0001 C CNN
F 1 "GND" H 7005 3727 50  0000 C CNN
F 2 "" H 7000 3900 50  0001 C CNN
F 3 "" H 7000 3900 50  0001 C CNN
	1    7000 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 3700 6900 4050
Wire Wire Line
	7000 4050 7000 3900
Wire Wire Line
	7000 5250 7000 5550
Wire Wire Line
	7000 5550 7300 5550
Wire Wire Line
	7100 5250 7100 5400
Wire Wire Line
	7100 5400 7300 5400
Text GLabel 7300 5550 2    50   Input ~ 0
RX
Text GLabel 7300 5400 2    50   Input ~ 0
TX
Wire Wire Line
	6900 5550 6900 5250
Text GLabel 5650 4700 1    50   Input ~ 0
TX_GND_817
$Comp
L Connector_Generic:Conn_01x04 J24
U 1 1 5CDA466A
P 8850 4100
F 0 "J24" H 8930 4092 50  0000 L CNN
F 1 "Conn_01x04" H 8930 4001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 8850 4100 50  0001 C CNN
F 3 "~" H 8850 4100 50  0001 C CNN
	1    8850 4100
	1    0    0    -1  
$EndComp
Text GLabel 8650 4000 0    50   Input ~ 0
COM_TRX
Text GLabel 8650 4100 0    50   Input ~ 0
RX
Text GLabel 8650 4200 0    50   Input ~ 0
TX
$Comp
L power:GND #PWR0114
U 1 1 5CDA5118
P 8650 4300
F 0 "#PWR0114" H 8650 4050 50  0001 C CNN
F 1 "GND" H 8655 4127 50  0000 C CNN
F 2 "" H 8650 4300 50  0001 C CNN
F 3 "" H 8650 4300 50  0001 C CNN
	1    8650 4300
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4148 D1
U 1 1 5CDA624E
P 5850 4550
F 0 "D1" V 5804 4629 50  0000 L CNN
F 1 "1N4148" V 5895 4629 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5850 4375 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5850 4550 50  0001 C CNN
	1    5850 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 4700 6350 4700
Connection ~ 5850 4700
Wire Wire Line
	5850 4400 6200 4400
Connection ~ 6200 4400
Wire Wire Line
	6200 4300 6200 4400
Wire Wire Line
	6350 4500 6350 4700
$Comp
L Connector_Generic:Conn_01x02 J10
U 1 1 5CDB52FE
P 950 2600
F 0 "J10" H 868 2817 50  0000 C CNN
F 1 "Conn_01x02" H 868 2726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 950 2600 50  0001 C CNN
F 3 "~" H 950 2600 50  0001 C CNN
	1    950  2600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1150 2600 1300 2600
Connection ~ 1300 2600
$Comp
L power:GND #PWR0117
U 1 1 5CDB662D
P 1250 2800
F 0 "#PWR0117" H 1250 2550 50  0001 C CNN
F 1 "GND" H 1255 2627 50  0000 C CNN
F 2 "" H 1250 2800 50  0001 C CNN
F 3 "" H 1250 2800 50  0001 C CNN
	1    1250 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2700 1250 2700
Wire Wire Line
	1250 2700 1250 2800
$Comp
L power:+12V #PWR0113
U 1 1 5CDBD7D8
P 6900 5550
F 0 "#PWR0113" H 6900 5400 50  0001 C CNN
F 1 "+12V" H 6915 5723 50  0000 C CNN
F 2 "" H 6900 5550 50  0001 C CNN
F 3 "" H 6900 5550 50  0001 C CNN
	1    6900 5550
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0118
U 1 1 5CE3D6A1
P 5650 2100
F 0 "#PWR0118" H 5650 1950 50  0001 C CNN
F 1 "+5V" H 5665 2273 50  0000 C CNN
F 2 "" H 5650 2100 50  0001 C CNN
F 3 "" H 5650 2100 50  0001 C CNN
	1    5650 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2150 5650 2100
$Comp
L power:GND #PWR0119
U 1 1 5CE4761D
P 5650 3050
F 0 "#PWR0119" H 5650 2800 50  0001 C CNN
F 1 "GND" H 5655 2877 50  0000 C CNN
F 2 "" H 5650 3050 50  0001 C CNN
F 3 "" H 5650 3050 50  0001 C CNN
	1    5650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3050 5650 2900
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5EFAD6B7
P 5750 2700
F 0 "J2" V 5714 2412 50  0000 R CNN
F 1 "Conn_01x04" V 5623 2412 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5750 2700 50  0001 C CNN
F 3 "~" H 5750 2700 50  0001 C CNN
	1    5750 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 2150 5750 2150
Wire Wire Line
	5650 2900 5750 2900
Connection ~ 5650 2900
Wire Wire Line
	5850 2900 5750 2900
Connection ~ 5750 2900
Wire Wire Line
	5850 2900 5950 2900
Connection ~ 5850 2900
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5EFB7264
P 5650 2350
F 0 "J1" H 5568 2567 50  0000 C CNN
F 1 "Conn_01x02" H 5568 2476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5650 2350 50  0001 C CNN
F 3 "~" H 5650 2350 50  0001 C CNN
	1    5650 2350
	0    -1   1    0   
$EndComp
Connection ~ 5650 2150
$Comp
L power:+12V #PWR0101
U 1 1 5EFBCF58
P 6800 2050
F 0 "#PWR0101" H 6800 1900 50  0001 C CNN
F 1 "+12V" H 6815 2223 50  0000 C CNN
F 2 "" H 6800 2050 50  0001 C CNN
F 3 "" H 6800 2050 50  0001 C CNN
	1    6800 2050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5EFBCF64
P 6800 2350
F 0 "J3" H 6718 2567 50  0000 C CNN
F 1 "Conn_01x02" H 6718 2476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6800 2350 50  0001 C CNN
F 3 "~" H 6800 2350 50  0001 C CNN
	1    6800 2350
	0    -1   1    0   
$EndComp
Wire Wire Line
	6800 2050 6800 2150
Wire Wire Line
	6900 2150 6800 2150
Connection ~ 6800 2150
$Comp
L Regulator_Linear:L7808 U1
U 1 1 5EFCEE56
P 2250 2600
F 0 "U1" H 2250 2842 50  0000 C CNN
F 1 "L7808" H 2250 2751 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2275 2450 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2250 2550 50  0001 C CNN
	1    2250 2600
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:L7805 U2
U 1 1 5EFCFA2C
P 3300 2600
F 0 "U2" H 3300 2842 50  0000 C CNN
F 1 "L7805" H 3300 2751 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 3325 2450 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 3300 2550 50  0001 C CNN
	1    3300 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5EFE342E
P 4950 4700
F 0 "J4" H 4868 4917 50  0000 C CNN
F 1 "Conn_01x02" H 4868 4826 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4950 4700 50  0001 C CNN
F 3 "~" H 4950 4700 50  0001 C CNN
	1    4950 4700
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5EFE343A
P 5250 4900
F 0 "#PWR0115" H 5250 4650 50  0001 C CNN
F 1 "GND" H 5255 4727 50  0000 C CNN
F 2 "" H 5250 4900 50  0001 C CNN
F 3 "" H 5250 4900 50  0001 C CNN
	1    5250 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 4800 5250 4800
Wire Wire Line
	5250 4800 5250 4900
Wire Wire Line
	5150 4700 5850 4700
$EndSCHEMATC
