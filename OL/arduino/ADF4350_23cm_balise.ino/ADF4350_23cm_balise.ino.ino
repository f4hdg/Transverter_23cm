//   ADF4251 and Arduino For 23 cm Transverter
//
//   Code ADF4350 By Alain Fort F1CJN feb 2,2016
//   Code Simple CW Beacon by Charlie Davy M0PZT  www.m0pzt.com 23rd December 2014
//   Fork by Loic F4HDG
//   update may 01, 2018
//
// Beacon on 23 cm band...
//
//


#include <SPI.h>

#define ADF4351_LE  3
#define MORSELEDPIN 1 
int     MorseWPM = 16;

// Declare variables...
#define strBeacondID "BEACON F4HDG JN25UE "


// Bung Morse into an array
char* MorseCodeCharacters[] = {
  "A", ".-",
  "B", "-...",
  "C", "-.-.",
  "D", "-..",
  "E", ".",
  "F", "..-.",
  "G", "--.",
  "H", "....",
  "I", "..",
  "J", ".---",
  "K", "-.-",
  "L", ".-..",
  "M", "--",
  "N", "-.",
  "O", "---",
  "P", ".--.",
  "Q", "--.-",
  "R", ".-.",
  "S", "...",
  "T", "-",
  "U", "..-",
  "V", "...-",
  "W", ".--",
  "X", "-..-",
  "Y", "-.--",
  "Z", "--..",
  "0", "-----",
  "1", ".----",
  "2", "..---",
  "3", "...--",
  "4", "....-",
  "5", ".....",
  "6", "-....",
  "7", "--...",
  "8", "---..",
  "9", "----.",
  "/", "-..-.",
  "*", "-.-.-",
  ".", ".-.-.",
  "&", "...-.-",
  " ", " "
};


uint32_t registers[6] =  {0x4580A8, 0x80080C9, 0x4E42, 0x4B3, 0xBC803C, 0x580005} ; // 437 MHz avec ref à 25 MHz

double RFfreq, PFDRFfreq, OutputChannelSpacing;
bool RFout;
// D2 D1 RFpow OUTPUT POWER
// 0  0   0      -4
// 0  1   1      -1
// 1  0   2      +2
// 1  1   3      +5
int RFpow;

void WriteRegister32(const uint32_t value)   //Programme un registre 32bits
{
  digitalWrite(ADF4351_LE, LOW);
  for (int i = 3; i >= 0; i--)          // boucle sur 4 x 8bits
    SPI.transfer((value >> 8 * i) & 0xFF); // décalage, masquage de l'octet et envoi via SPI
  digitalWrite(ADF4351_LE, HIGH);
  digitalWrite(ADF4351_LE, LOW);
}

void SetADF4351()  // Programme tous les registres de l'ADF4351
{
  double INT, FRACF;
  unsigned int long INTA, RFcalc, MOD, FRAC;
  byte OutputDivider;

  if (RFfreq >= 2200) {
    OutputDivider = 1;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 2200) {
    OutputDivider = 2;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 1100) {
    OutputDivider = 4;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 550)  {
    OutputDivider = 8;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 275)  {
    OutputDivider = 16;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 137.5) {
    OutputDivider = 32;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 68.75) {
    OutputDivider = 64;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 0);
  }

  INTA = (RFfreq * OutputDivider) / PFDRFfreq;
  MOD = (PFDRFfreq / OutputChannelSpacing);
  FRACF = (((RFfreq * OutputDivider) / PFDRFfreq) - INTA) * MOD;
  FRAC = round(FRACF); // On arrondit le résultat

  registers[0] = 0;
  registers[0] = INTA << 15; // OK
  FRAC = FRAC << 3;
  registers[0] = registers[0] + FRAC;

  registers[1] = 0;
  registers[1] = MOD << 3;
  registers[1] = registers[1] + 1 ; // ajout de l'adresse "001"
  bitSet (registers[1], 27); // Prescaler sur 8/9

  if (RFout)
    bitClear(registers[2], 5);
  else
    bitSet(registers[2], 5);

  bitSet (registers[2], 28); // Digital lock == "110" sur b28 b27 b26
  bitSet (registers[2], 27); // digital lock
  bitClear (registers[2], 26); // digital lock


  switch (RFpow)
  {
    case 0 :
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 0);
      break;
    case 1 :
      bitWrite (registers[4], 3, 1);
      bitWrite (registers[4], 4, 0);
      break;
    case 2 :
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 1);
      break;
    case 3 :
      bitWrite (registers[4], 3, 1);
      bitWrite (registers[4], 4, 1);
      break;
    default:
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 0);
  }

  for (int i = 5; i >= 0; i--)  // programmation ADF4351 en commencant par R5
    WriteRegister32(registers[i]);
}

//************************************ Balise ****************************************
void Balise(String MorseString, int MorseWPM) {

  Serial.print("Message: ");
  Serial.println(MorseString);

  int CWdot = 1200 / MorseWPM;
  int CWdash = (1200 / MorseWPM) * 3;
  int istr;

  //// Boucle message
  for (istr = 0; istr < MorseString.length(); istr++) {
    String currentchar = MorseString.substring(istr, istr + 1);
    int ichar = 0;

    ///// Rechche du character dans la table des codes
    while (ichar < sizeof(MorseCodeCharacters)) {
      String currentletter = MorseCodeCharacters[ichar];   // letter
      String currentcode = MorseCodeCharacters[ichar + 1]; // corresponding morse code
      if (currentletter.equalsIgnoreCase(currentchar)) {

        ///// Boucle du charactere envoye
        int icp = 0;
        for (icp = 0; icp < currentcode.length(); icp++) {
          // Transmit Dit
          if (currentcode.substring(icp, icp + 1).equalsIgnoreCase(".")) {
            digitalWrite(MORSELEDPIN, HIGH);
            //////// ADF4350
            RFout = true;
            SetADF4351();
            delay(CWdot);
            digitalWrite(MORSELEDPIN, LOW);
            //////// ADF4350
            RFout = false;
            SetADF4351();
            delay(CWdot);
          }
          // Transmit Dah
          else if (currentcode.substring(icp, icp + 1).equalsIgnoreCase("-")) {
            digitalWrite(MORSELEDPIN, HIGH);
            //////// ADF4350
            RFout = true;
            SetADF4351();
            delay(CWdash);
            digitalWrite(MORSELEDPIN, LOW);
            //////// ADF4350
            RFout = false;
            SetADF4351();
            delay(CWdot);
          }
          else if (currentcode.substring(icp, icp + 1).equalsIgnoreCase(" ")) {
            delay(CWdot * 3);
          };
        }
      }
      ichar = ichar + 2;
    }
    delay(CWdot * 3);
  }
  delay(CWdot * 7);
 
  /// Envoie d'une porteuse de 5s
  digitalWrite(MORSELEDPIN, HIGH);
  //////// ADF4350
  RFout = true;
  SetADF4351();
  delay(5000);
  digitalWrite(MORSELEDPIN, LOW);
  //////// ADF4350
  RFout = false;
  SetADF4351();

}

//************************************ Setup ****************************************
void setup() {

  Serial.begin (19200); //  Serial to the PC via Arduino "Serial Monitor"  at 9600

  pinMode(2, INPUT);  // PIN 2 en entree pour lock
  pinMode(ADF4351_LE, OUTPUT);          // Setup pins
  digitalWrite(ADF4351_LE, HIGH);
  SPI.begin();                          // Init SPI bus
  SPI.setDataMode(SPI_MODE0);           // CPHA = 0 et Clock positive
  SPI.setBitOrder(MSBFIRST);            // poids forts en tête

  PFDRFfreq= 25;
  RFfreq = 1296;
  RFpow = 0;
  OutputChannelSpacing = 0.0001; // Pas de fréquence = 10kHz
  RFout = false;

  SetADF4351();  // Programme tous les registres de l'ADF4351
  RFout = true;
  delay(1000);
  SetADF4351();

} // Fin setup

//*************************************Loop***********************************
void loop()
{
  String pwr_txt;
  if      (RFpow  == 0) pwr_txt = " -4dBm";
  else if (RFpow  == 1) pwr_txt = " -1dBm";
  else if (RFpow  == 2) pwr_txt = " +2dBm";
  else if (RFpow  == 3) pwr_txt = " +5dBm";


  //  digitalWrite(pinPTT, HIGH);
  Balise(strBeacondID + pwr_txt, MorseWPM);
  //  digitalWrite(pinPTT, LOW);
  delay(5000);

  RFpow = RFpow + 1;
  if (RFpow > 3)
    RFpow = 0;


}   // fin loop
