//   ADF4251 and Arduino For 23 cm Transverter
//
//   By Alain Fort F1CJN feb 2,2016
//   Fork by Loic F4HDG
//   update march 25, 2018
//
// Cette version permet la génération d'un signal simple à 1152 MHz pour un transverter 144MHz/1296MHz
//
//


#include <SPI.h>

#define ADF4351_LE 7

uint32_t registers[6] =  {0x4580A8, 0x80080C9, 0x4E42, 0x4B3, 0xBC803C, 0x580005} ; // 437 MHz avec ref à 25 MHz

double RFfreq, PFDRFfreq, OutputChannelSpacing;
bool RFout;
// D2 D1 RFpow OUTPUT POWER
// 0  0   0      -4
// 0  1   1      -1
// 1  0   2      +2
// 1  1   3      +5
int RFpow;

void WriteRegister32(const uint32_t value)   //Programme un registre 32bits
{
  digitalWrite(ADF4351_LE, LOW);
  for (int i = 3; i >= 0; i--)          // boucle sur 4 x 8bits
  {
    SPI.transfer((value >> 8 * i) & 0xFF); // décalage, masquage de l'octet et envoi via SPI

  }
  digitalWrite(ADF4351_LE, HIGH);
  digitalWrite(ADF4351_LE, LOW);
}

void SetADF4351()  // Programme tous les registres de l'ADF4351
{
  double FRACF;
  unsigned int long INTA, FRAC, MOD;
  byte OutputDivider;

  if (RFfreq >= 2200) {
    OutputDivider = 1;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 2200) {
    OutputDivider = 2;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 1100) {
    OutputDivider = 4;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 550)  {
    OutputDivider = 8;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 275)  {
    OutputDivider = 16;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 137.5) {
    OutputDivider = 32;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 68.75) {
    OutputDivider = 64;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 0);
  }

  INTA = (RFfreq * OutputDivider) / PFDRFfreq;
  MOD = (PFDRFfreq / OutputChannelSpacing);
  FRACF = (((RFfreq * OutputDivider) / PFDRFfreq) - INTA) * MOD;
  FRAC = round(FRACF); // On arrondit le résultat

/*
  Serial.print("Freq RF: div : ");
  Serial.println((INTA + (FRAC/MOD))*(PFDRFfreq/OutputDivider));
  
  Serial.print("Freq RF ");
  Serial.println((INTA + (double(FRAC)/MOD))*(PFDRFfreq/OutputDivider));
  
  Serial.print("int : ");
  Serial.println((INTA ));
  Serial.print("FRAC :  ");
  Serial.println(FRAC);
  Serial.print("MOD : ");
  Serial.println(MOD);
  Serial.print("RFin: ");
  Serial.println(PFDRFfreq);
  Serial.print("div : ");
  Serial.println(OutputDivider);
  
*/

  registers[0] = 0;
  registers[0] = INTA << 15; // OK
  FRAC = FRAC << 3;
  registers[0] = registers[0] + FRAC;

  registers[1] = 0;
  registers[1] = MOD << 3;
  registers[1] = registers[1] + 1 ; // ajout de l'adresse "001"
  bitSet (registers[1], 27); // Prescaler sur 8/9

  if (RFout)
    bitClear(registers[2], 5);
  else
    bitSet(registers[2], 5);

  bitSet (registers[2], 28); // Digital lock == "110" sur b28 b27 b26
  bitSet (registers[2], 27); // digital lock
  bitClear (registers[2], 26); // digital lock


  switch (RFpow)
  {
    case 0 :
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 0);
      break;
    case 1 :
      bitWrite (registers[4], 3, 1);
      bitWrite (registers[4], 4, 0);
      break;
    case 2 :
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 1);
      break;
    case 3 :
      bitWrite (registers[4], 3, 1);
      bitWrite (registers[4], 4, 1);
      break;
    default:
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 0);
  }

  for (int i = 5; i >= 0; i--)  // programmation ADF4351 en commencant par R5
    WriteRegister32(registers[i]);

}


//************************************ Setup ****************************************
void setup() {

//  Serial.begin (19200); //  Serial to the PC via Arduino "Serial Monitor"  at 9600
//  Serial.println("Serial...");
  pinMode(2, INPUT);  // PIN 2 en entree pour lock
  pinMode(ADF4351_LE, OUTPUT);          // Setup pins
  digitalWrite(ADF4351_LE, HIGH);
  SPI.begin();                          // Init SPI bus
  SPI.setDataMode(SPI_MODE0);           // CPHA = 0 et Clock positive
  SPI.setBitOrder(MSBFIRST);            // poids forts en tête
/*  Serial.println(" Registre init");
  Serial.println(" RegX 1234567890123456789012345678901");
  
  for (int i = 5; i >= 0; i--)  // programmation ADF4351 en commencant par R5
  {
    Serial.print(" Reg");
    Serial.print(i);
    Serial.print(" ");
    Serial.print(registers[i], BIN);
    Serial.println(" ");

  }
*/

  PFDRFfreq = 25;
  RFfreq = 1152;
  RFpow = 2;
  OutputChannelSpacing = 0.01; // Pas de fréquence = 10kHz
  RFout = false;
//  Serial.println("Init...");
  // SetADF4351();  // Programme tous les registres de l'ADF4351
  RFout = true;
  delay(1000);
  SetADF4351();

  /*for (int i = 5; i >= 0; i--)  // programmation ADF4351 en commencant par R5
    WriteRegister32(registers[i]);

  Serial.println(" Registre programme");
  for (int i = 5; i >= 0; i--)  // programmation ADF4351 en commencant par R5
  {
    Serial.print(" Reg");
    Serial.print(i);
    Serial.print(" ");
    Serial.print(registers[i], BIN);
    Serial.println(" ");

  }*/


} // Fin setup

//*************************************Loop***********************************
void loop()
{
  /*
    delay(5000);

    RFpow = RFpow + 1;
    if (RFpow > 3)
      RFpow = 0;

    SetADF4351();

  */
  //Serial.println("LOOOP");
}   // fin loop
